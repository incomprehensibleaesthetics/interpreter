# Interpreter

A simple (and incomplete) Pascal interpreter, built (loosely) following a fantastic guide by Ruslan Spivak:

https://ruslanspivak.com/lsbasi-part1/


## Current State

The interpreter currently supports:

- basic syntax checking
- a few semantic checks, such as requiring variables to be declared before being referenced
- basic arithmetic (`+`, `-`, `*`, `DIV`, `/` and parentheses)
- `PROGRAM` statement
- variable declarations (`VAR`)
- variable assignments (via `:=`)
- `INTEGER` and `REAL` types
- compound statements (denoted with `BEGIN` and `END`, can also be nested)
- procedure declarations with or without parameters (can also be nested)
- non-nested procedure calls
- nested procedure calls are currently WIP (they work as long as they only reference local variables and parameters)


## Dependencies

Make sure [Elixir](https://elixir-lang.org/install.html) and `mix` are installed.


## Running

### Executable

Compile and run the executable, like so:

```sh
# The --stack option isn't required but currently it's the only way to see that
# the code does anything at all.
$ mix escript.build && ./interpreter --stack /path/to/code.pas
```

### Via iex

You can use the interpreter to run Pascal code via `iex`, like so:

```sh
$ iex -S mix
iex(1)> code = """
...(1)> PROGRAM hello;
...(1)>   VAR num: INTEGER;
...(1)>
...(1)>   BEGIN
...(1)>     num := 7 + 3 * (10 DIV (12 DIV (3 + 1) - 1)) DIV (2 + 3) - 5 - 3 + (8)
...(1)>   END.
...(1)> """
"PROGRAM hello;\n  VAR num: INTEGER;\n\n  BEGIN\n    num := 7 + 3 * (10 DIV (12 DIV (3 + 1) - 1)) DIV (2 + 3) - 5 - 3 + (8)\n  END.\n"
...(2)> Pascal.Runner.run(code, stack: true)
ENTER: PROGRAM hello
CALL STACK
1: PROGRAM hello

LEAVE: PROGRAM hello
CALL STACK
1: PROGRAM hello
   num                 : 10
:ok
```

The runner prints the call stack (when invoked with `stack: true`) and returns `:ok` if the code was executed successfully.
