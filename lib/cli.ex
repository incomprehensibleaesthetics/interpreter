defmodule CLI do
  alias Pascal.Runner

  def main(args) do
    # TODO: Add help switch.
    available_opts = [switches: [stack: :boolean]]
    {call_opts, positional, _invalid} = OptionParser.parse(args, available_opts)

    # TODO: Handle invalid args.
    # TODO: Handle bad number of positional args.

    case File.read(positional |> hd()) do
      {:ok, code} -> Runner.run(code, call_opts)
      {:error, reason} -> IO.puts(:stderr, ["Error opening file:\n", to_string(reason)])
    end
  end
end
