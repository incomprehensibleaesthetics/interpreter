defmodule GenParser do
  @moduledoc """
  Provides a parser behaviour and utility functions to support building a token
  parser.
  """

  alias __MODULE__.Error
  alias GenLexer.Token

  @type ast :: any()

  @callback parse(list(Token.t())) :: {:ok, ast()} | {:error, Error.t()}
  @callback parse!(list(Token.t())) :: ast()

  defmacro __using__(opts) do
    ast_mod = Keyword.fetch!(opts, :ast_mod)

    quote do
      @behaviour GenParser

      @typedoc """
      Desribes a sequence of tokens as a list of tuples that each look like this

      - `{:token, t} # t is the name of the token (terminal)`
      - `{:rule, r}  # r is the name of the rule (non-terminal)`

      For example, an assignment statement can be expressed as the following sequence:

      ```
      [token: :id, token: :assign, rule: :expression, token: :semi]
      ```
      """
      @type seq_desc :: list({:token | :rule, atom()})

      @typedoc """
      An item that is part of a sequence.
      """
      @type seq_item :: unquote(ast_mod).t() | Token.t() | sequence()

      @typedoc """
      Represents a sequence of tokens.
      """
      @type sequence :: list(seq_item())

      @doc """
      Consumes the specified sequence.
      """
      @spec seq(list(Token.t()), seq_desc()) ::
              {:ok, {sequence(), list(Token.t())}} | {:error, Error.t()}
      def seq(input_tokens, seq_symbols) do
        r =
          Enum.reduce_while(seq_symbols, {:ok, {[], input_tokens}}, fn
            {sym_type, sym_name}, {:ok, {collected, unprocessed}} ->
              extract_symbol(unprocessed, sym_type, sym_name, collected)

            _, {:error, _} = err ->
              err
          end)

        case r do
          {:ok, {items, unprocessed_tokens}} -> {:ok, {Enum.reverse(items), unprocessed_tokens}}
          {:error, _} = err -> err
        end
      end

      @doc """
      Updates the return value of `seq` to have a flattened item list.
      """
      @spec flatten_seq({:ok, {sequence(), list(Token.t())}} | {:error, Error.t()}) ::
              {:ok, {sequence(), list(Token.t())}} | {:error, Error.t()}
      def flatten_seq({:ok, {items, tokens}}), do: {:ok, {List.flatten(items), tokens}}
      def flatten_seq({:error, _} = err), do: err

      @doc """
      Updates the return value of `seq` to only include the item at the nth
      position, instead of the entire list of items.

      The returned item is `nil` if there is nothing at the specified position.
      """
      @spec seq_item_at({:ok, {sequence(), list(Token.t())}} | {:error, Error.t()}, integer()) ::
              {:ok, {seq_item() | nil, list(Token.t())}} | {:error, Error.t()}
      def seq_item_at({:ok, {items, tokens}}, pos), do: {:ok, {Enum.at(items, pos), tokens}}
      def seq_item_at({:error, _} = err, _pos), do: err

      # Consumes the specified token.
      @spec extract_symbol(list(Token.t()), :token, atom(), list(seq_item())) ::
              {:cont | :halt, {:ok, {list(Token.t()), list(Token.t())}} | {:error, Error.t()}}
      defp extract_symbol(unprocessed_tokens, :token, token_name, collected) do
        case eat_token(unprocessed_tokens, token_name) do
          {:ok, {new, unprocessed_tokens}} ->
            {:cont, {:ok, {[new | collected], unprocessed_tokens}}}

          {:error, _} = err ->
            {:halt, err}
        end
      end

      # Consumes the specified rule.
      @spec extract_symbol(list(Token.t()), :rule, atom(), list(seq_item())) ::
              {:cont | :halt,
               {:ok, {list(Token.t() | unquote(ast_mod).t()), list(Token.t())}}
               | {:error, Error.t()}}
      defp extract_symbol(unprocessed_tokens, :rule, rule_name, collected) do
        case parse_inner(unprocessed_tokens, rule_name) do
          {:ok, {new, unprocessed_tokens}} ->
            {:cont, {:ok, {[new | collected], unprocessed_tokens}}}

          {:error, _} = err ->
            {:halt, err}
        end
      end

      @doc """
      Consumes the specified sequence, returning an empty list instead of an
      error if the passed token sequence didn't start with the specified sequence.
      """
      @spec optional_seq(list(Token.t()), seq_desc()) ::
              {:ok, {sequence(), list(Token.t() | unquote(ast_mod).t())}}
      def optional_seq(tokens, seq_symbols) do
        case seq(tokens, seq_symbols) do
          {:ok, {items, tokens}} = ret_val -> ret_val
          {:error, err} -> {:ok, {[], tokens}}
        end
      end

      @doc """
      Consumes the specified sequence as many times as possible.
      """
      @spec repeat_seq(list(Token.t()), seq_desc()) ::
              {:ok, {sequence(), list(Token.t() | unquote(ast_mod).t())}}
      def repeat_seq(tokens, seq_symbols) do
        case seq(tokens, seq_symbols) do
          {:ok, {items, tokens}} ->
            {:ok, {more_items, tokens}} = repeat_seq(tokens, seq_symbols)
            {:ok, {items ++ more_items, tokens}}

          {:error, err} ->
            # IO.puts("canceling repeat: #{inspect err}")
            # IO.puts("  seq: #{inspect seq_symbols}")
            # IO.puts("  err: #{inspect err}")
            # IO.puts("  tok: #{inspect tokens}")
            {:ok, {[], tokens}}
        end
      end

      @doc """
      Consumes the specified sequence as many times as possible but at least once.
      """
      @spec repeat_seq(list(Token.t()), seq_desc(), :at_least_once) ::
              {:ok, {sequence(), list(Token.t() | unquote(ast_mod).t())}} | {:error, Error.t()}
      def repeat_seq(tokens, seq_symbols, :at_least_once) do
        with {:ok, {hd_seq, tokens}} <- seq(tokens, seq_symbols),
             {:ok, {tl_seqs, tokens}} <- repeat_seq(tokens, seq_symbols),
             do: {:ok, {hd_seq ++ tl_seqs, tokens}}
      end

      @doc """
      Consumes a single token of the specified type.
      """
      @spec eat_token(list(Token.t()), atom()) ::
              {:ok, {Token.t(), list(Token.t())}} | {:error, Error.t()}

      def eat_token([], token_name) do
        unmet_expectation = "#{token_str(token_name)}"
        {:error, Error.new(:unexpected_eof, unmet_expectation: unmet_expectation)}
      end

      def eat_token([token = %Token{name: token_name} | remaining_tokens], token_name),
        do: {:ok, {token, remaining_tokens}}

      def eat_token([token | _], token_name) do
        unmet_expectation = "#{token_str(token_name)}"
        {:error, Error.new(:bad_syntax, bad_token: token, unmet_expectation: unmet_expectation)}
      end
    end
  end
end
