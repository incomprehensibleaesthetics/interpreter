defmodule ActivationRecord do
  @moduledoc """
  A single activation record (aka call stack frame).
  """
  defstruct [:name, :type, :nesting_level, members: %{}]

  @type t :: %__MODULE__{}

  @type record_type :: :procedure | :program

  @doc """
  Returns a new ActivationRecord struct.
  """
  @spec new(String.t(), record_type(), integer()) :: t()
  def new(name, type, nesting_level) do
    %__MODULE__{
      name: name,
      type: type,
      nesting_level: nesting_level,
      members: %{}
    }
  end

  @doc """
  Fetches the specified member.
  """
  @spec fetch_member(t(), String.t()) :: {:ok, any()} | :error
  def fetch_member(subj, member_name) do
    Map.fetch(subj.members, member_name)
  end

  @doc """
  Inserts or updates the specified member.
  """
  @spec put_member(t(), String.t(), any()) :: t()
  def put_member(subj, member_name, val) do
    %__MODULE__{subj | members: Map.put(subj.members, member_name, val)}
  end
end

defimpl String.Chars, for: ActivationRecord do
  def to_string(record) do
    member_strings =
      record.members
      |> Enum.map(fn {name, val} -> "   #{String.pad_trailing(name, 20)}: #{val}" end)

    type_str =
      record.type
      |> Kernel.to_string()
      |> String.upcase()

    ["#{record.nesting_level}: #{type_str} #{record.name}" | member_strings]
    |> Enum.join("\n")
  end
end
