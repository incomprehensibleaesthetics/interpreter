defmodule Pascal.SemanticAnalyzer.Error do
  @moduledoc """
  A semantic error.
  """
  defexception [:name, :token]

  alias GenLexer.Token

  @type t :: %__MODULE__{}

  @doc """
  Returns a new GenParser.Error struct.
  """
  @spec new(atom(), token: Token.t()) :: t()
  def new(name, token: token) do
    %__MODULE__{
      name: name,
      token: token
    }
  end

  def message(err) do
    case err.name do
      :undeclared_var ->
        "undeclared variable ('#{err.token.str}') on line #{err.token.line_no} at column #{
          err.token.col_no
        }"

      :duplicate_var_declaration ->
        "duplicate variable declaration ('#{err.token.str}') on line #{err.token.line_no} at column #{
          err.token.col_no
        }"

      :invalid_args ->
        "the procedure #{err.token.str} was called with the wrong number of arguments on line #{
          err.token.line_no
        } at column #{err.token.col_no}"

      _ ->
        "unknown semantic error '#{err.name}' on line #{err.token.line_no} at column #{
          err.token.col_no
        }"
    end
  end
end
