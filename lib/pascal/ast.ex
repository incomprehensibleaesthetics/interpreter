defmodule Pascal.Ast do
  alias __MODULE__.Assign
  alias __MODULE__.BinOp
  alias __MODULE__.Block
  alias __MODULE__.Compound
  alias __MODULE__.NoOp
  alias __MODULE__.Num
  alias __MODULE__.Param
  alias __MODULE__.ProcDecl
  alias __MODULE__.Program
  alias __MODULE__.UnaryOp
  alias __MODULE__.Type
  alias __MODULE__.Var
  alias __MODULE__.VarDecl

  @typedoc """
  An abstract syntax tree.
  """
  @type t ::
          Block.t()
          | Param.t()
          | ProcDecl.t()
          | Program.t()
          | Type.t()
          | VarDecl.t()
          | expr()
          | statement()
          | list(t())

  @typedoc """
  Any kind of expression.
  """
  @type expr :: BinOp.t() | Num.t() | UnaryOp.t() | Var.t()

  @typedoc """
  Any kind of statement.
  """
  @type statement :: Assign.t() | Compound.t() | NoOp.t()
end
