defmodule Pascal.Interpreter do
  @moduledoc """
  Provides functions for interpreting an abstract syntax tree for Pascal code.

  ## Example

      valid_ast = Pascal.Ast.Program.new(
        # ...
      )

      Interpreter.exec!(valid_ast)
      
  """
  defstruct [:call_stack, :options]

  alias Pascal.Ast
  alias Ast.Assign
  alias Ast.BinOp
  alias Ast.Block
  alias Ast.Compound
  alias Ast.NoOp
  alias Ast.Num
  alias Ast.Param
  alias Ast.ProcCall
  alias Ast.ProcDecl
  alias Ast.Program
  alias Ast.Type
  alias Ast.UnaryOp
  alias Ast.Var
  alias Ast.VarDecl

  alias GenLexer.Token

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Interpreter struct.
  """
  @spec new(keyword()) :: t()
  def new(opts \\ []) do
    %__MODULE__{
      call_stack: CallStack.new(),
      options: opts
    }
  end

  @doc """
  Executes the specified abstract syntax tree.
  """
  @spec exec!(Ast.t(), keyword()) :: :ok
  def exec!(ast, opts \\ []) do
    new(opts) |> eval!(ast)

    :ok
  end

  @doc """
  Evaluates the specified abstract syntax tree, returning a potential return
  value and an updated interpreter.
  """
  @spec eval!(t(), Ast.t()) :: {any(), t()}

  def eval!(subj, %Assign{left: %Var{token: %Token{str: id}}, right: expr}) do
    # Evaluate the right side first.
    {val, subj} = eval!(subj, expr)

    # Update the callstack.
    call_stack =
      CallStack.update_hd(subj.call_stack, fn record ->
        ActivationRecord.put_member(record, id, val)
      end)

    # Return an updated subject.
    {nil, %__MODULE__{subj | call_stack: call_stack}}
  end

  def eval!(subj, %BinOp{left: left, op: %Token{name: op}, right: right}) do
    {left_val, subj} = eval!(subj, left)
    {right_val, subj} = eval!(subj, right)
    result = apply_bin_op(left_val, op, right_val)
    {result, subj}
  end

  def eval!(subj, %Block{compound: compound_statement}) do
    {_, subj} = eval!(subj, compound_statement)
    {nil, subj}
  end

  def eval!(input_subj, %Compound{children: statements}) do
    output_subj =
      Enum.reduce(statements, input_subj, fn statement, subj ->
        subj
        |> eval!(statement)
        |> elem(1)
      end)

    {nil, output_subj}
  end

  def eval!(subj, %NoOp{}), do: {nil, subj}

  def eval!(subj, %Num{token: %Token{name: :integer_const, str: str}}) do
    {val, ""} = Integer.parse(str)
    {val, subj}
  end

  def eval!(subj, %Num{token: %Token{name: :real_const, str: str}}) do
    {val, ""} = Float.parse(str)
    {val, subj}
  end

  def eval!(subj, %Param{}), do: {nil, subj}

  def eval!(input_subj, %ProcCall{name_token: %Token{str: name}, args: args, symbol: proc_sym}) do
    subj =
      input_subj
      |> push_new_record_to_stack(name, :procedure)

    # Add the arguments from the call to the stack.
    subj =
      Enum.zip(proc_sym.params, args)
      |> Enum.reduce(subj, fn {param_sym, arg_node}, subj ->
        # Evaluate out the actual value for the argument.
        {val, subj} = eval!(subj, arg_node)

        # Write the value to the current activation record on the stack.
        update_activation_record(subj, fn record ->
          ActivationRecord.put_member(record, param_sym.name, val)
        end)
      end)

    log(subj, "ENTER: PROCEDURE #{name}")
    log(subj, to_string(subj.call_stack))

    # Evaluate the block.
    {_, updated_subj} = eval!(subj, proc_sym.block)

    log(updated_subj, "\nLEAVE: PROCEDURE #{name}")
    log(updated_subj, to_string(updated_subj.call_stack))

    # TODO: Think about whether it's really ok to simply return the input
    #   subject unchanged.
    {nil, input_subj}
  end

  def eval!(subj, %ProcDecl{}), do: {nil, subj}

  def eval!(input_subj, %Program{name: %Token{str: name}, block: block}) do
    log(input_subj, "ENTER: PROGRAM #{name}")

    # Add a new activation record to the call stack.
    subj = push_new_record_to_stack(input_subj, name, :program)

    log(subj, to_string(subj.call_stack))

    # Evaluate the inner block.
    {_, updated_subj} = eval!(subj, block)

    log(updated_subj, "\nLEAVE: PROGRAM #{name}")
    log(updated_subj, to_string(updated_subj.call_stack))

    # TODO: Think about whether it's really ok to simply return the input
    #   subject unchanged.
    {nil, input_subj}
  end

  def eval!(subj, %Type{}), do: {nil, subj}

  def eval!(subj, %UnaryOp{op: %Token{name: op}, val: expr}) do
    {val, subj} = eval!(subj, expr)
    result = apply_unary_op(op, val)
    {result, subj}
  end

  def eval!(subj, %Var{token: %Token{str: name}}) do
    {:ok, val} = fetch(subj, name)
    {val, subj}
  end

  def eval!(subj, %VarDecl{}), do: {nil, subj}

  @doc """
  Returns the value of the specified variable.
  """
  @spec fetch(t(), String.t()) :: {:ok, any()} | :error
  def fetch(subj, var_name) do
    {record, _} = subj.call_stack |> CallStack.pop()
    ActivationRecord.fetch_member(record, var_name)
  end

  # Creates a new record and pushes it onto the stack.
  @spec push_new_record_to_stack(t(), String.t(), ActivationRecord.record_type()) :: t()
  defp push_new_record_to_stack(subj, record_name, record_type) do
    updated_call_stack =
      subj.call_stack
      |> CallStack.push_new(record_name, record_type)

    Map.put(subj, :call_stack, updated_call_stack)
  end

  # Updates the current activation record.
  @spec update_activation_record(t(), function()) :: t()
  defp update_activation_record(subj, fun) do
    Map.put(subj, :call_stack, CallStack.update_hd(subj.call_stack, fun))
  end

  @spec apply_bin_op(number(), atom(), number()) :: number()
  defp apply_bin_op(left, :plus, right), do: left + right
  defp apply_bin_op(left, :minus, right), do: left - right
  defp apply_bin_op(left, :mul, right), do: left * right
  defp apply_bin_op(left, :integer_div, right), do: div(left, right)
  defp apply_bin_op(left, :float_div, right), do: left / right

  @spec apply_unary_op(atom(), number()) :: number()
  defp apply_unary_op(:plus, val), do: val
  defp apply_unary_op(:minus, val), do: -val

  @spec log(t(), String.t()) :: :ok
  defp log(%__MODULE__{options: opts}, msg) do
    case Keyword.fetch(opts, :stack) do
      {:ok, true} -> IO.puts(msg)
      _ -> :ok
    end
  end
end
