defmodule Pascal.Parser do
  @moduledoc """
  Provides functions for parsing sequences of pascal tokens.
  """

  alias Pascal.Ast
  alias Ast.Assign
  alias Ast.BinOp
  alias Ast.Block
  alias Ast.Compound
  alias Ast.NoOp
  alias Ast.Num
  alias Ast.Param
  alias Ast.ProcCall
  alias Ast.ProcDecl
  alias Ast.Program
  alias Ast.Type
  alias Ast.UnaryOp
  alias Ast.Var
  alias Ast.VarDecl

  use GenParser, ast_mod: Ast

  alias GenParser.Error
  alias GenLexer.Token

  @doc """
  Parses a sequence of tokens.
  """
  @impl GenParser
  @spec parse(list(Token.t()), atom()) :: {:ok, Ast.t()} | {:error, Error.t()}
  def parse(tokens, start_symbol \\ :program) do
    with {:ok, {ast, unprocessed_tokens}} <- parse_inner(tokens, start_symbol) do
      case unprocessed_tokens do
        [%Token{name: :eof}] -> {:ok, ast}
        [] -> {:error, Error.new(:missing_eof)}
        [token | _] -> {:error, Error.new(:trailing_code, bad_token: token)}
      end
    end
  end

  @doc """
  Parses a sequence of tokens.

  Raises an error if parsing fails.
  """
  @impl GenParser
  @spec parse!(list(Token.t()), atom()) :: Ast.t()
  def parse!(tokens, start_symbol \\ :program) do
    case parse(tokens, start_symbol) do
      {:ok, ast} -> ast
      {:error, err} -> raise err
    end
  end

  # Parses a sequence of tokens without expecting a final eof token.
  @spec parse_inner(list(Token.t()), atom()) ::
          {:ok, {Ast.t(), list(Token.t())}} | {:error, Error.t()}

  # assignment_statement : variable ASSIGN expr
  defp parse_inner(tokens, :assignment_statement) do
    with {:ok, {left, tokens}} <- parse_inner(tokens, :variable),
         {:ok, {op, tokens}} <- eat_token(tokens, :assign),
         {:ok, {right, tokens}} <- parse_inner(tokens, :expr),
         do: {:ok, {Assign.new(left, op, right), tokens}}
  end

  # block : declarations compound_statement
  defp parse_inner(tokens, :block) do
    with {:ok, {declarations, tokens}} <- parse_inner(tokens, :declarations),
         {:ok, {compound, tokens}} <- parse_inner(tokens, :compound_statement),
         do: {:ok, {Block.new(declarations, compound), tokens}}
  end

  # compound_statement : BEGIN statement_list END
  defp parse_inner(tokens, :compound_statement) do
    with {:ok, {_, tokens}} <- eat_token(tokens, :begin),
         {:ok, {statements, tokens}} <- parse_inner(tokens, :statement_list),
         {:ok, {_, tokens}} <- eat_token(tokens, :end),
         do: {:ok, {Compound.new(statements), tokens}}
  end

  # variable_declarations : variable_declaration_line* procedure_declaration*
  defp parse_inner(tokens, :declarations) do
    with {:ok, {var_decl_nodes, tokens}} <- repeat_seq(tokens, rule: :variable_declaration_line),
         {:ok, {proc_decl_nodes, tokens}} <- repeat_seq(tokens, rule: :procedure_declaration) do
      decl_nodes =
        (var_decl_nodes ++ proc_decl_nodes)
        |> List.flatten()

      {:ok, {decl_nodes, tokens}}
    end
  end

  # empty:
  defp parse_inner(tokens, :empty), do: {:ok, {NoOp.new(), tokens}}

  # expr : term ((PLUS | MINUS) term)*
  defp parse_inner(tokens, :expr) do
    with {:ok, {first_term, tokens}} <- parse_inner(tokens, :term),
         {:ok, {additional_items, tokens}} <- repeat_seq(tokens, rule: :expr_op, rule: :term) do
      if Enum.empty?(additional_items) do
        {:ok, {first_term, tokens}}
      else
        node =
          additional_items
          |> Enum.chunk_every(2)
          |> Enum.reduce(first_term, fn [op, right], left -> BinOp.new(left, op, right) end)

        {:ok, {node, tokens}}
      end
    end
  end

  # expr_op : PLUS | MINUS
  #
  # TODO: Consider renaming this. It doesn't return a true ast.
  defp parse_inner(tokens, :expr_op) do
    with {:error, _} <- eat_token(tokens, :plus),
         {:error, _} <- eat_token(tokens, :minus) do
      # TODO: Handle an empty list of tokens.
      token = hd(tokens)

      {:error, Error.new(:bad_syntax, bad_token: token, unmet_expectation: "'+' or '-'")}
    end
  end

  # factor : unary_factor
  #        | num_literal
  #        | LPAREN expr RPAREN
  #        | variable
  defp parse_inner(tokens, :factor) do
    with {:error, _} <- parse_inner(tokens, :unary_factor),
         {:error, _} <- parse_inner(tokens, :num_literal),
         {:error, _} <-
           seq(tokens, token: :lparen, rule: :expr, token: :rparen) |> seq_item_at(1),
         {:error, _} <- parse_inner(tokens, :variable) do
      # TODO: Handle an empty list of tokens.
      token = hd(tokens)

      {:error,
       Error.new(:bad_syntax,
         bad_token: token,
         unmet_expectation: "a valid factor (e.g. a number or a variable)"
       )}
    end
  end

  # formal_parameter_list : formal_parameters (SEMI formal_parameter_list)*
  defp parse_inner(tokens, :formal_parameter_list) do
    with {:ok, {first_params, tokens}} <- parse_inner(tokens, :formal_parameters),
         {:ok, {additional_items, tokens}} <-
           repeat_seq(tokens, token: :semi, rule: :formal_parameter_list) do
      additional_params =
        additional_items
        |> Enum.drop_every(2)
        |> List.flatten()

      {:ok, {first_params ++ additional_params, tokens}}
    end
  end

  # formal_parameters : ID (COMMA ID)* COLON type_spec
  defp parse_inner(tokens, :formal_parameters) do
    with {:ok, {first_name, tokens}} <- eat_token(tokens, :id),
         {:ok, {additional_tokens, tokens}} <- repeat_seq(tokens, token: :comma, token: :id),
         {:ok, {_, tokens}} <- eat_token(tokens, :colon),
         {:ok, {type, tokens}} <- parse_inner(tokens, :type_spec) do
      additional_names = additional_tokens |> Enum.drop_every(2)

      decl_nodes =
        [first_name | additional_names]
        |> Enum.map(fn name -> Param.new(name, type) end)

      {:ok, {decl_nodes, tokens}}
    end
  end

  # num_literal : INTEGER_CONST | REAL_CONST
  defp parse_inner(tokens, :num_literal) do
    with {:error, _} <- eat_token(tokens, :integer_const),
         {:error, _} <- eat_token(tokens, :real_const) do
      # TODO: Handle an empty list of tokens.
      token = hd(tokens)

      {:error, Error.new(:bad_syntax, bad_token: token, unmet_expectation: "a number")}
    else
      {:ok, {token, tokens}} -> {:ok, {Num.new(token), tokens}}
    end
  end

  # args_list : (expr (COMMA expr)*)?
  defp parse_inner(tokens, :args_list) do
    case parse_inner(tokens, :expr) do
      {:ok, {hd_arg, tokens}} ->
        {:ok, {items, tokens}} = repeat_seq(tokens, token: :comma, rule: :expr)
        tl_args = items |> Enum.drop_every(2)
        {:ok, {[hd_arg | tl_args], tokens}}

      {:error, _} ->
        {:ok, {[], tokens}}
    end
  end

  # procedure_call_statement : ID LPAREN (expr (COMMA expr)*)? RPAREN
  defp parse_inner(tokens, :procedure_call_statement) do
    with {:ok, {proc_name, tokens}} <- eat_token(tokens, :id),
         {:ok, {_, tokens}} <- eat_token(tokens, :lparen),
         {:ok, {args, tokens}} <- parse_inner(tokens, :args_list),
         {:ok, {_, tokens}} <- eat_token(tokens, :rparen),
         do: {:ok, {ProcCall.new(proc_name, args), tokens}}
  end

  # procedure_declaration : PROCEDURE ID (LPAREN formal_parameter_list RPAREN)? SEMI block SEMI
  defp parse_inner(tokens, :procedure_declaration) do
    with {:ok, {_, tokens}} <- eat_token(tokens, :procedure),
         {:ok, {name, tokens}} <- eat_token(tokens, :id),
         {:ok, {params, tokens}} <-
           optional_seq(tokens, token: :lparen, rule: :formal_parameter_list, token: :rparen)
           |> seq_item_at(1),
         {:ok, {_, tokens}} <- eat_token(tokens, :semi),
         {:ok, {block, tokens}} <- parse_inner(tokens, :block),
         {:ok, {_, tokens}} <- eat_token(tokens, :semi),
         do: {:ok, {ProcDecl.new(name, params || [], block), tokens}}
  end

  # program : PROGRAM variable SEMI block DOT
  defp parse_inner(tokens, :program) do
    with {:ok, {_, tokens}} <- eat_token(tokens, :program),
         {:ok, {name, tokens}} <- parse_inner(tokens, :variable),
         {:ok, {_, tokens}} <- eat_token(tokens, :semi),
         {:ok, {block, tokens}} <- parse_inner(tokens, :block),
         {:ok, {_, tokens}} <- eat_token(tokens, :dot),
         do: {:ok, {Program.new(name.token, block), tokens}}
  end

  # statement : compound_statement
  #           | assignment_statement
  #           | procedure_call_statement
  #           | empty
  defp parse_inner(tokens, :statement) do
    with {:error, _} <- parse_inner(tokens, :compound_statement),
         {:error, _} <- parse_inner(tokens, :assignment_statement),
         {:error, _} <- parse_inner(tokens, :procedure_call_statement),
         do: parse_inner(tokens, :empty)
  end

  # statement_list : statement (SEMI statement)*
  defp parse_inner(tokens, :statement_list) do
    with {:ok, {first_statement, tokens}} <- parse_inner(tokens, :statement),
         {:ok, {additional_items, tokens}} <- repeat_seq(tokens, token: :semi, rule: :statement) do
      # Remove semi tokens.
      additional_statements = additional_items |> Enum.drop_every(2)

      {:ok, {[first_statement | additional_statements], tokens}}
    end
  end

  # term : factor ((MUL | INTEGER_DIV | FLOAT_DIV) factor)*
  defp parse_inner(tokens, :term) do
    with {:ok, {first_factor, tokens}} <- parse_inner(tokens, :factor),
         {:ok, {additional_items, tokens}} <- repeat_seq(tokens, rule: :term_op, rule: :factor) do
      if Enum.empty?(additional_items) do
        {:ok, {first_factor, tokens}}
      else
        node =
          additional_items
          |> Enum.chunk_every(2)
          |> Enum.reduce(first_factor, fn [op, right], left -> BinOp.new(left, op, right) end)

        {:ok, {node, tokens}}
      end
    end
  end

  # term_op : MUL | INTEGER_DIV | FLOAT_DIV
  #
  # TODO: Consider renaming this. It doesn't return a true ast.
  defp parse_inner(tokens, :term_op) do
    with {:error, _} <- eat_token(tokens, :mul),
         {:error, _} <- eat_token(tokens, :float_div),
         {:error, _} <- eat_token(tokens, :integer_div) do
      # TODO: Handle an empty list of tokens.
      token = hd(tokens)

      {:error, Error.new(:bad_syntax, bad_token: token, unmet_expectation: "'*', '/' or 'DIV'")}
    end
  end

  # type_spec : INTEGER | REAL
  defp parse_inner(tokens, :type_spec) do
    with {:error, _} <- eat_token(tokens, :integer),
         {:error, _} <- eat_token(tokens, :real) do
      # TODO: Handle an empty list of tokens.
      token = hd(tokens)

      {:error,
       Error.new(:bad_syntax, bad_token: token, unmet_expectation: "a type (e.g. 'INTEGER')")}
    else
      {:ok, {token, remaining_tokens}} -> {:ok, {Type.new(token), remaining_tokens}}
    end
  end

  # unary_factor : (PLUS | MINUS) factor
  defp parse_inner(tokens, :unary_factor) do
    with {:error, _} <- seq(tokens, token: :plus, rule: :factor),
         {:error, _} <- seq(tokens, token: :minus, rule: :factor) do
      # TODO: Handle an empty list of tokens.
      token = hd(tokens)

      {:error,
       Error.new(:bad_syntax, bad_token: token, unmet_expectation: "a unary operator ('+' or '-')")}
    else
      {:ok, {[op, factor], tokens}} -> {:ok, {UnaryOp.new(op, factor), tokens}}
    end
  end

  # variable : ID
  defp parse_inner(tokens, :variable) do
    with {:ok, {id, tokens}} <- eat_token(tokens, :id), do: {:ok, {Var.new(id), tokens}}
  end

  # variable_declaration : ID (COMMA ID)* COLON type_spec
  defp parse_inner(tokens, :variable_declaration) do
    with {:ok, {first_name, tokens}} <- eat_token(tokens, :id),
         {:ok, {additional_tokens, tokens}} <- repeat_seq(tokens, token: :comma, token: :id),
         {:ok, {_, tokens}} <- eat_token(tokens, :colon),
         {:ok, {type, tokens}} <- parse_inner(tokens, :type_spec) do
      # Drop comma tokens.
      additional_names = additional_tokens |> Enum.drop_every(2)

      declarations =
        Enum.map([first_name | additional_names], fn name ->
          VarDecl.new(name, type)
        end)

      {:ok, {declarations, tokens}}
    end
  end

  # variable_declaration_line : VAR (variable_declaration SEMI)+
  defp parse_inner(tokens, :variable_declaration_line) do
    with {:ok, {_, tokens}} <- eat_token(tokens, :var) do
      seq_desc = [rule: :variable_declaration, token: :semi]

      with {:ok, {parsed_items, tokens}} <- repeat_seq(tokens, seq_desc, :at_least_once) do
        # Remove semi tokens.
        decl_nodes =
          [nil | parsed_items]
          |> Enum.drop_every(2)
          |> List.flatten()

        {:ok, {decl_nodes, tokens}}
      end
    end
  end

  @spec token_str(atom()) :: String.t()
  defp token_str(:assign), do: "':='"
  defp token_str(:begin), do: "'BEGIN'"
  defp token_str(:colon), do: "':'"
  defp token_str(:comma), do: "','"
  defp token_str(:dot), do: "'.'"
  defp token_str(:end), do: "'END'"
  defp token_str(:float_div), do: "'/'"
  defp token_str(:id), do: "an identifier"
  defp token_str(:integer), do: "'INTEGER'"
  defp token_str(:integer_const), do: "an integer"
  defp token_str(:integer_div), do: "'DIV'"
  defp token_str(:lparen), do: "'('"
  defp token_str(:minus), do: "'-'"
  defp token_str(:mul), do: "'*'"
  defp token_str(:plus), do: "'+'"
  defp token_str(:procedure), do: "'PROCEDURE'"
  defp token_str(:program), do: "'PROGRAM'"
  defp token_str(:real), do: "'REAL'"
  defp token_str(:real_const), do: "a real number"
  defp token_str(:rparen), do: "')'"
  defp token_str(:semi), do: "';'"
  defp token_str(:var), do: "'VAR'"
end
