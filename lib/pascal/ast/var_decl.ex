defmodule Pascal.Ast.VarDecl do
  @moduledoc """
  A variable declaration node of an abstract syntax tree with a name and type node.
  """
  defstruct [:name, :type]

  alias GenLexer.Token
  alias Pascal.Ast.Type

  @type t :: %__MODULE__{}

  @doc """
  Returns a new VarDecl struct.
  """
  @spec new(Token.t(), Type.t()) :: t()
  def new(name_token, type_node) do
    %__MODULE__{name: name_token, type: type_node}
  end
end
