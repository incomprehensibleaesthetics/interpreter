defmodule Pascal.Ast.UnaryOp do
  @moduledoc """
  A unary operation node of an abstract syntax tree with an operation token and
  a value sub-tree.
  """
  defstruct [:op, :val]

  alias GenLexer.Token
  alias Pascal.Ast

  @type t :: %__MODULE__{}

  @doc """
  Returns a new UnaryOp struct.
  """
  @spec new(Token.t(), Ast.expr()) :: t()
  def new(op, val) do
    %__MODULE__{op: op, val: val}
  end
end
