defmodule Pascal.Ast.ProcDecl do
  @moduledoc """
  A procedure declaration node of an abstract syntax tree with a name and block node.
  """
  defstruct [:name_token, :params, :block]

  alias GenLexer.Token
  alias Pascal.Ast.Block
  alias Pascal.Ast.Param

  @type t :: %__MODULE__{}

  @doc """
  Returns a new ProcDecl struct.
  """
  @spec new(Token.t(), list(Param.t()), Block.t()) :: t()
  def new(name_token, params, block) do
    %__MODULE__{name_token: name_token, params: params, block: block}
  end
end
