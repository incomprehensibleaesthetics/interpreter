defmodule Pascal.Ast.Compound do
  @moduledoc """
  A compound statement node of an abstract syntax tree with child sub-trees.
  """
  defstruct [:children]

  alias Pascal.Ast

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Compound struct.
  """
  @spec new(list(Ast.t())) :: t()
  def new(children) do
    %__MODULE__{children: children}
  end
end
