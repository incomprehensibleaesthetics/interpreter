defmodule Pascal.Ast.Block do
  @moduledoc """
  A block node of an abstract syntax tree with declarations and a compound statement.
  """
  defstruct [:declarations, :compound]

  alias Pascal.Ast.Compound
  alias Pascal.Ast.VarDecl

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Block struct.
  """
  @spec new(list(VarDecl.t()), Compound.t()) :: t()
  def new(declarations, compound_statement) do
    %__MODULE__{declarations: declarations, compound: compound_statement}
  end
end
