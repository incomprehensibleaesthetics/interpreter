defmodule Pascal.Ast.Var do
  @moduledoc """
  A variable node of an abstract syntax tree with an id token.
  """
  defstruct [:token]

  alias GenLexer.Token

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Var struct.
  """
  @spec new(Token.t()) :: t()
  def new(token) do
    %__MODULE__{token: token}
  end
end
