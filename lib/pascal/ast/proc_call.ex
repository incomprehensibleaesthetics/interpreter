defmodule Pascal.Ast.ProcCall do
  @moduledoc """
  A procedure call node of an abstract syntax tree.
  """
  defstruct [:name_token, :args, :symbol]

  alias GenLexer.Token
  alias Pascal.Ast.Param
  alias Pascal.Symbol.ProcSymbol

  @type t :: %__MODULE__{}

  @doc """
  Returns a new ProcCall struct.
  """
  @spec new(Token.t(), list(Param.t()), ProcSymbol.t() | nil) :: t()
  def new(name_token, args, proc_sym \\ nil) do
    %__MODULE__{
      name_token: name_token,
      args: args,
      symbol: proc_sym
    }
  end
end
