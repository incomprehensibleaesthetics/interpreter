defmodule Pascal.Ast.NoOp do
  @moduledoc """
  A no-op node of an abstract syntax tree.
  """
  defstruct []

  @type t :: %__MODULE__{}

  @doc """
  Returns a new NoOp struct.
  """
  @spec new() :: t()
  def new(), do: %__MODULE__{}
end
