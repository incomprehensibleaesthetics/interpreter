defmodule Pascal.Ast.Assign do
  @moduledoc """
  An assignment operation node of an abstract syntax tree with a left and right
  node and an operation token.
  """
  defstruct [:left, :op, :right]

  alias GenLexer.Token
  alias Pascal.Ast
  alias Pascal.Ast.Var

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Assign struct.
  """
  @spec new(Var.t(), Token.t(), Ast.expr()) :: t()
  def new(left, op, right) do
    %__MODULE__{left: left, op: op, right: right}
  end
end
