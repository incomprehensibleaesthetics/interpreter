defmodule Pascal.Ast.Param do
  @moduledoc """
  A formal parameter node of an abstract syntax tree with a name and type node.
  """
  defstruct [:name_token, :type]

  alias GenLexer.Token
  alias Pascal.Ast.Type

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Param struct.
  """
  @spec new(Token.t(), Type.t()) :: t()
  def new(name_token, type_node) do
    %__MODULE__{name_token: name_token, type: type_node}
  end
end
