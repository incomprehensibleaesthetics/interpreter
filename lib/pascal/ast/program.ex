defmodule Pascal.Ast.Program do
  @moduledoc """
  A program node of an abstract syntax tree with a name and a block.
  """
  defstruct [:name, :block]

  alias GenLexer.Token
  alias Pascal.Ast.Block

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Program struct.
  """
  @spec new(Token.t(), Block.t()) :: t()
  def new(name_token, block) do
    %__MODULE__{name: name_token, block: block}
  end
end
