defmodule Pascal.Ast.BinOp do
  @moduledoc """
  A binary operation node of an abstract syntax tree with a left and right
  node and an operation token.
  """
  defstruct [:left, :op, :right]

  alias GenLexer.Token
  alias Pascal.Ast

  @type t :: %__MODULE__{}

  @doc """
  Returns a new BinOp struct.
  """
  @spec new(Ast.t(), Token.t(), Ast.t()) :: t()
  def new(left, op, right) do
    %__MODULE__{left: left, op: op, right: right}
  end
end
