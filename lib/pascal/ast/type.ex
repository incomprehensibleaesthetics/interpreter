defmodule Pascal.Ast.Type do
  @moduledoc """
  A type node of an abstract syntax tree.
  """
  defstruct [:token]

  alias GenLexer.Token

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Type struct.
  """
  @spec new(Token.t()) :: t()
  def new(token) do
    %__MODULE__{token: token}
  end
end
