defmodule Pascal.SemanticAnalyzer do
  @moduledoc """
  Provides functions for building a symbol table from an abstract syntax tree.
  """

  alias __MODULE__.Error

  alias Pascal.Ast
  alias Ast.Assign
  alias Ast.BinOp
  alias Ast.Block
  alias Ast.Compound
  alias Ast.NoOp
  alias Ast.Num
  alias Ast.Param
  alias Ast.ProcCall
  alias Ast.ProcDecl
  alias Ast.Program
  alias Ast.Type
  alias Ast.UnaryOp
  alias Ast.Var
  alias Ast.VarDecl

  alias GenLexer.Token
  alias Pascal.ScopedSymbolTable

  alias Pascal.Symbol
  alias Symbol.BuiltInTypeSymbol
  alias Symbol.ProcSymbol
  alias Symbol.VarSymbol

  @typedoc """
  The internal state, including the current scope.
  """
  @type state_t :: %{current_scope: ScopedSymbolTable.t() | nil}

  @doc """
  Analyzes the passed abstract syntax tree
  """
  @spec analyze(Ast.t()) :: :ok | {:error, Error.t()}
  def analyze(ast) do
    with {:ok, _} <- visit(ast, %{current_scope: create_builtins_scope()}),
         do: :ok
  end

  @spec create_builtins_scope() :: ScopedSymbolTable.t()
  defp create_builtins_scope do
    ScopedSymbolTable.new("builtins", 0)
    |> add_built_in_types()
  end

  @spec add_built_in_types(ScopedSymbolTable.t()) :: ScopedSymbolTable.t()
  defp add_built_in_types(scope) do
    scope = ScopedSymbolTable.define(scope, BuiltInTypeSymbol.new("INTEGER"))
    ScopedSymbolTable.define(scope, BuiltInTypeSymbol.new("REAL"))
  end

  @doc """
  Visits the passed ast node and updates the symbol table accordingly.

  Returns an error if referenced variables were not in the symbol table.
  """
  @spec visit(Ast.t(), state_t()) :: {:ok, state_t()} | {:error, Error.t()}

  def visit(%Assign{left: left, right: right}, state) do
    with {:ok, state} <- visit(left, state), do: visit(right, state)
  end

  def visit(%BinOp{left: left, right: right}, state) do
    with {:ok, state} <- visit(left, state), do: visit(right, state)
  end

  def visit(%Block{declarations: declarations, compound: compound}, state) do
    with {:ok, state} <- visit_nodes(declarations, state), do: visit(compound, state)
  end

  def visit(%Compound{children: children}, state), do: visit_nodes(children, state)

  def visit(%NoOp{}, state), do: {:ok, state}

  def visit(%Num{}, state), do: {:ok, state}

  def visit(
        %Param{name_token: %Token{str: name}, type: %Type{token: %Token{str: type}}},
        state = %{current_scope: scope}
      ) do
    with {:ok, type_symbol} <- ScopedSymbolTable.lookup(scope, type) do
      var_symbol = VarSymbol.new(name, type_symbol)
      updated_scope = ScopedSymbolTable.define(scope, var_symbol)
      {:ok, %{state | current_scope: updated_scope}}
    end
  end

  def visit(%ProcCall{name_token: token, args: args}, state) do
    with {:ok, %ProcSymbol{params: params}} <-
           ScopedSymbolTable.lookup(state.current_scope, token.str) do
      param_count = Enum.count(params)
      arg_count = Enum.count(args)

      if param_count == arg_count do
        {:ok, state}
      else
        {:error, Error.new(:invalid_args, token: token)}
      end
    else
      {:error, _} -> {:error, Error.new(:undeclared_proc, token: token)}
    end
  end

  def visit(
        %ProcDecl{name_token: %Token{str: name}, params: params, block: block},
        input_state = %{current_scope: input_outer_scope}
      ) do
    %ScopedSymbolTable{scope_level: outer_scope_level} = input_outer_scope

    # Add a symbol for this procedure to the current scope.
    proc_symbol = ProcSymbol.new(name, params, block)
    updated_outer_scope = ScopedSymbolTable.define(input_outer_scope, proc_symbol)

    # Create and enter a new scope.

    # IO.puts("ENTER scope: #{name}")

    proc_scope =
      ScopedSymbolTable.new(name, outer_scope_level + 1)
      |> Map.put(:enclosing_scope, updated_outer_scope)

    initial_proc_state = Map.put(input_state, :current_scope, proc_scope)

    # Visit the parameters, then continue traversing the tree.
    with {:ok, proc_state_with_params} <- visit_nodes(params, initial_proc_state),
         {:ok, _final_proc_state} <- visit(block, proc_state_with_params) do
      # log_scope(final_proc_state)
      # IO.puts("LEAVE scope: #{name}")

      # The input state should not have changed, except for the proc symbol added
      # to the scope.
      {:ok, %{input_state | current_scope: updated_outer_scope}}
    end
  end

  def visit(%Program{block: block}, state = %{current_scope: scope}) do
    # IO.puts("ENTER scope: global")
    %ScopedSymbolTable{scope_level: outer_scope_level} = scope
    global_scope = ScopedSymbolTable.new("global", outer_scope_level + 1, scope)
    state = Map.put(state, :current_scope, global_scope)

    with {:ok, final_state} <- visit(block, state) do
      # log_scope(final_state)
      # IO.puts("LEAVE scope: global")
      {:ok, final_state}
    end
  end

  def visit(%UnaryOp{val: val}, state), do: visit(val, state)

  def visit(%Var{token: token}, state = %{current_scope: scope}) do
    case ScopedSymbolTable.lookup(scope, token.str) do
      {:ok, _} -> {:ok, state}
      {:error, _} -> {:error, Error.new(:undeclared_var, token: token)}
    end
  end

  def visit(
        %VarDecl{name: token = %Token{str: name}, type: %Type{token: %Token{str: type}}},
        state = %{current_scope: scope}
      ) do
    with {:ok, type_symbol} <- ScopedSymbolTable.lookup(scope, type) do
      # Check whether the variable was already declared *within this scope*. If
      # it was declared in an enclosing scope, it will be shadowed.
      case ScopedSymbolTable.lookup(scope, name, false) do
        # TODO: Differentiate between different symbol categories.
        {:ok, _} ->
          {:error, Error.new(:duplicate_var_declaration, token: token)}

        {:error, _} ->
          var_symbol = VarSymbol.new(name, type_symbol)
          updated_scope = ScopedSymbolTable.define(scope, var_symbol)
          {:ok, %{state | current_scope: updated_scope}}
      end
    end
  end

  # Visits each node from the passed list, updating the state.
  #
  # Returns the new state or the first encountered error.
  @spec visit_nodes([Ast.t()], state_t()) :: {:ok, state_t()} | {:error, Error.t()}
  defp visit_nodes(nodes, input_state) do
    Enum.reduce_while(nodes, {:ok, input_state}, fn
      node, {:ok, state} -> {:cont, visit(node, state)}
      _, {:error, _} = err -> {:halt, err}
    end)
  end

  @doc """
  Prints the curent scope table.
  """
  @spec log_scope(state_t()) :: :ok
  def log_scope(%{current_scope: scope}) do
    IO.puts("")
    IO.puts(scope)
  end
end
