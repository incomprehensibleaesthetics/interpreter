defmodule Pascal.Symbol do
  alias __MODULE__.BuiltInTypeSymbol
  alias __MODULE__.ParamSymbol
  alias __MODULE__.ProcSymbol
  alias __MODULE__.VarSymbol

  @typedoc """
  Any kind of Pascal symbol.
  """
  @type t :: BuiltInTypeSymbol.t() | ParamSymbol.t() | ProcSymbol.t() | VarSymbol.t()
end
