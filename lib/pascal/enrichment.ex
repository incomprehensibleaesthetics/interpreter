defmodule Pascal.Enrichment do
  @moduledoc """
  Provides functions for enriching an abstract syntax tree.
  """

  alias Pascal.Ast
  alias Ast.Assign
  alias Ast.BinOp
  alias Ast.Block
  alias Ast.Compound
  alias Ast.NoOp
  alias Ast.Num
  alias Ast.ProcCall
  alias Ast.ProcDecl
  alias Ast.Program
  alias Ast.UnaryOp
  alias Ast.Var
  alias Ast.VarDecl

  alias GenLexer.Token
  alias Pascal.ScopedSymbolTable

  alias Pascal.Symbol
  alias Symbol.BuiltInTypeSymbol
  alias Symbol.ParamSymbol
  alias Symbol.ProcSymbol

  @typedoc """
  The internal state, including the current scope.
  """
  @type state_t :: %{current_scope: ScopedSymbolTable.t()}

  @spec create_builtins_scope() :: ScopedSymbolTable.t()
  defp create_builtins_scope do
    ScopedSymbolTable.new("builtins", 0)
    |> add_built_in_types()
  end

  @spec add_built_in_types(ScopedSymbolTable.t()) :: ScopedSymbolTable.t()
  defp add_built_in_types(scope) do
    scope
    |> ScopedSymbolTable.define(BuiltInTypeSymbol.new("INTEGER"))
    |> ScopedSymbolTable.define(BuiltInTypeSymbol.new("REAL"))
  end

  @doc """
  Enriches the passed abstract syntax tree with additional data.

  Expects an ast without semantic errors.

  The data added to the tree is:

  - ProcCall: symbol
  """
  @spec enrich!(Ast.t()) :: Ast.t()
  def enrich!(ast) do
    state = %{current_scope: create_builtins_scope()}
    with {ast, _state} <- visit(ast, state), do: ast
  end

  @spec visit(Ast.t(), state_t()) :: {Ast.t(), state_t()}

  defp visit(%Assign{} = input_ast, state) do
    {right, state} = visit(input_ast.right, state)
    {left, state} = visit(input_ast.left, state)
    {%Assign{input_ast | left: left, right: right}, state}
  end

  defp visit(%BinOp{} = input_ast, state) do
    {right, state} = visit(input_ast.right, state)
    {left, state} = visit(input_ast.left, state)
    {%BinOp{input_ast | left: left, right: right}, state}
  end

  defp visit(%Block{} = input_ast, state) do
    {declarations, state} = visit_nodes(input_ast.declarations, state)
    {compound, state} = visit(input_ast.compound, state)
    {%Block{input_ast | declarations: declarations, compound: compound}, state}
  end

  defp visit(%Compound{} = input_ast, state) do
    {children, state} = visit_nodes(input_ast.children, state)
    {%Compound{input_ast | children: children}, state}
  end

  defp visit(%NoOp{} = input_ast, state), do: {input_ast, state}

  defp visit(%Num{} = input_ast, state), do: {input_ast, state}

  defp visit(%ProcCall{name_token: %Token{str: name}} = input_ast, state) do
    {:ok, sym} = ScopedSymbolTable.lookup(state.current_scope, name)
    {%ProcCall{input_ast | symbol: sym}, state}
  end

  defp visit(
         %ProcDecl{name_token: name_token, params: param_nodes, block: block} = input_ast,
         state
       ) do
    # Visit sub-trees first because they may contain nested procedure calls,
    # etc.. And if they do, they might enrich their sub-trees too.
    {block, state} = visit(block, state)
    output_ast = Map.put(input_ast, :block, block)

    # TODO: Handle non-built-in types.
    param_symbols =
      param_nodes
      |> Enum.map(fn p ->
        ParamSymbol.new(p.name_token.str, BuiltInTypeSymbol.new(p.type.token.str))
      end)

    sym = ProcSymbol.new(name_token.str, param_symbols, block)
    state = state |> define_symbol(sym)

    {output_ast, state}
  end

  defp visit(%Program{} = input_ast, state) do
    {block, state} = visit(input_ast.block, state)
    {%Program{input_ast | block: block}, state}
  end

  defp visit(%UnaryOp{} = input_ast, state) do
    {val, state} = visit(input_ast.val, state)
    {%UnaryOp{input_ast | val: val}, state}
  end

  defp visit(%Var{} = input_ast, state), do: {input_ast, state}

  defp visit(%VarDecl{} = input_ast, state), do: {input_ast, state}

  @spec visit_nodes(list(Ast.t()), state_t()) :: {list(Ast.t()), state_t()}
  defp visit_nodes(input_nodes, input_state) do
    {output_nodes, output_state} =
      Enum.reduce(input_nodes, {[], input_state}, fn input_node, {output_nodes, state} ->
        {output_node, state} = visit(input_node, state)
        {[output_node | output_nodes], state}
      end)

    {Enum.reverse(output_nodes), output_state}
  end

  @spec define_symbol(state_t(), Symbol.t()) :: state_t()
  defp define_symbol(state, sym) do
    %{state | current_scope: ScopedSymbolTable.define(state.current_scope, sym)}
  end
end
