defmodule Pascal.Lexer do
  @moduledoc """
  A lexical analyzer.
  """

  @enforce_keys [:entire_code, :unprocessed_chars, :line_no, :col_no, :log_to]
  defstruct [:entire_code, :unprocessed_chars, line_no: 1, col_no: 1, log_to: :stderr]

  require GenLexer.Macros

  alias GenLexer.Error
  alias GenLexer.Macros
  alias GenLexer.Token

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Pascal.Lexer struct.
  """
  @spec new(String.t(), keyword()) :: t()
  def new(code, options \\ []) do
    chars = to_charlist(code)
    device = Keyword.get(options, :log_to, :stderr)

    %__MODULE__{
      entire_code: code,
      unprocessed_chars: chars,
      line_no: 1,
      col_no: 1,
      log_to: device
    }
  end

  @doc """
  Tokenizes the passed code and returns the tokens.
  """
  @spec tokenize(String.t(), keyword()) :: {:ok, list(Token.t())} | {:error, Error.t()}
  def tokenize(code, options \\ []) do
    try do
      tokens =
        __MODULE__.new(code, options)
        |> next_tokens!()
        |> Enum.to_list()

      {:ok, tokens}
    rescue
      err in Error -> {:error, err}
    end
  end

  @doc """
  Tokenizes the passed code and returns the tokens.

  Raises an error if tokenization fails.
  """
  @spec tokenize!(String.t(), keyword()) :: list(Token.t())
  def tokenize!(code, options \\ []) do
    code
    |> __MODULE__.new(options)
    |> next_tokens!()
    |> Enum.to_list()
  end

  @doc """
  Returns the next token.
  """
  @spec next_token(t()) :: {:ok, {Token.t(), t()}} | {:error, Error.t()}
  def next_token(lexer) do
    %__MODULE__{
      unprocessed_chars: chars,
      line_no: line_no,
      col_no: col_no
    } = lexer

    case chars do
      '' ->
        {:ok, {Token.new(:eof, "", pos: {line_no, col_no}), lexer}}

      _ ->
        with :error <- eat_token(chars, :whitespace),
             # Special Characters and Operators
             :error <- eat_token(chars, :plus),
             :error <- eat_token(chars, :minus),
             :error <- eat_token(chars, :mul),
             :error <- eat_token(chars, :float_div),
             :error <- eat_token(chars, :lparen),
             :error <- eat_token(chars, :rparen),
             :error <- eat_token(chars, :dot),
             :error <- eat_token(chars, :assign),
             :error <- eat_token(chars, :semi),
             :error <- eat_token(chars, :colon),
             :error <- eat_token(chars, :comma),
             # Reserved Keywords
             :error <- eat_token(chars, :begin),
             :error <- eat_token(chars, :integer_div),
             :error <- eat_token(chars, :end),
             :error <- eat_token(chars, :integer),
             :error <- eat_token(chars, :procedure),
             :error <- eat_token(chars, :program),
             :error <- eat_token(chars, :real),
             :error <- eat_token(chars, :var),
             # Dynamic Character Sequences
             :error <- eat_token(chars, :real_const),
             :error <- eat_token(chars, :integer_const),
             :error <- eat_token(chars, :id),
             :error <- eat_token(chars, :comment) do
          err = Error.new(:invalid_token, line_no, col_no)
          log_err(lexer, err)
          {:error, err}
        else
          {:ok, token = %Token{str: token_str}} ->
            token = Token.set_pos(token, line_no, col_no)
            lexer = advance_pointer(lexer, token_str)

            if ignored?(token) do
              next_token(lexer)
            else
              {:ok, {token, lexer}}
            end
        end
    end
  end

  @doc """
  Returns a stream of all remaining tokens.

  Raises an error if tokenization fails.
  """
  @spec next_tokens!(t()) :: Enumerable.t()
  def next_tokens!(lexer) do
    Stream.unfold(lexer, fn
      :halt ->
        nil

      lexer ->
        case next_token(lexer) do
          {:ok, {%Token{name: :eof} = token, _}} -> {token, :halt}
          {:ok, {token, lexer}} -> {token, lexer}
          {:error, err} -> raise err
        end
    end)
  end

  # Consumes all passed tokens sequentially.
  #
  # Note: The returned tokens don't have their position set.
  @spec eat_tokens(charlist(), list(atom())) :: {:ok, list(Token.t())} | :error

  defp eat_tokens(_chars, []), do: {:ok, []}

  defp eat_tokens(chars, [first_token_name | remaining_token_names]) do
    with {:ok, token} <- eat_token(chars, first_token_name) do
      r =
        chars
        |> Enum.drop(String.length(token.str))
        |> eat_tokens(remaining_token_names)

      case r do
        {:ok, other_tokens} -> {:ok, [token | other_tokens]}
        :error -> :error
      end
    end
  end

  # Consumes a single token of the specified type.
  #
  # Note: The returned token doesn't have its position set.
  @spec eat_token(charlist(), atom()) :: {:ok, Token.t()} | :error

  defp eat_token(chars, :id) do
    # Any potential id that isn't a reserved keyword is a valid id.
    with {:ok, %Token{name: :potential_id, str: id}} <- eat_token(chars, :potential_id),
         nil <- keyword_token(id) do
      {:ok, Token.new(:id, id)}
    else
      _ -> :error
    end
  end

  defp eat_token(chars, :real_const) do
    with {:ok, [%Token{str: pre}, _decimal, %Token{str: post}]} <-
           eat_tokens(chars, [:integer_const, :dot, :integer_const]),
         do: {:ok, Token.new(:real_const, "#{pre}.#{post}")}
  end

  Macros.def_symbol(:plus, '+')
  Macros.def_symbol(:minus, '-')
  Macros.def_symbol(:mul, '*')
  Macros.def_symbol(:float_div, '/')
  Macros.def_symbol(:lparen, '(')
  Macros.def_symbol(:rparen, ')')
  Macros.def_symbol(:dot, '.')
  Macros.def_symbol(:assign, ':=')
  Macros.def_symbol(:semi, ';')
  Macros.def_symbol(:colon, ':')
  Macros.def_symbol(:comma, ',')

  Macros.def_keyword(:begin, 'BEGIN')
  Macros.def_keyword(:integer_div, 'DIV')
  Macros.def_keyword(:end, 'END')
  Macros.def_keyword(:integer, 'INTEGER')
  Macros.def_keyword(:procedure, 'PROCEDURE')
  Macros.def_keyword(:program, 'PROGRAM')
  Macros.def_keyword(:real, 'REAL')
  Macros.def_keyword(:var, 'VAR')

  # Any kind of whitespace.
  Macros.def_char_seq(:whitespace, :whitespace?)

  # An integer literal.
  Macros.def_char_seq(:integer_const, :digit?)

  # A reserved keyword or an id.
  #
  # Any potential id that isn't a reserved keyword is an id.
  Macros.def_char_seq(:potential_id, :id_char?, first_char: :first_id_char?)

  # A comment token.
  #
  # Starts with a '{' and ends with a '}'.
  #
  # TODO: Make this macro handle characters as an alternative to a callback.
  Macros.def_char_seq(:comment, :any_char,
    first_char: :opening_comment_char?,
    last_char: :closing_comment_char?
  )

  # Returns a keyword token for the specified string or nil if the string
  # doesn't represent a reserved keyword.
  @spec keyword_token(String.t(), boolean()) :: Token.t() | nil
  defp keyword_token(word, downcase_first? \\ true)
  defp keyword_token(word, true), do: word |> String.downcase() |> keyword_token(false)
  defp keyword_token(_, false), do: nil

  # Advances the lexer's char pointer to account for the processed string.
  @spec advance_pointer(t(), String.t()) :: t()
  defp advance_pointer(lexer = %__MODULE__{col_no: input_col_no}, processed_str) do
    # TODO: Develop a more efficient way to do this.

    processed_char_count = String.length(processed_str)
    lines = String.split(processed_str, "\n")
    processed_line_count = length(lines) - 1

    output_col_no =
      if processed_line_count == 0 do
        input_col_no + processed_char_count
      else
        1 + (lines |> List.last() |> String.length())
      end

    lexer
    |> Map.update(:unprocessed_chars, '', fn chars -> Enum.drop(chars, processed_char_count) end)
    |> Map.update(:line_no, 1, fn l -> l + processed_line_count end)
    |> Map.put(:col_no, output_col_no)
  end

  @doc """
  Returns true if the passed token is ignored by `next_token/1`.
  """
  @spec ignored?(Token.t()) :: boolean()
  def ignored?(%Token{name: :whitespace}), do: true
  def ignored?(%Token{name: :comment}), do: true
  def ignored?(_), do: false

  @doc """
  Returns true if the passed character is a alphabetic.
  """
  @spec alpha?(char()) :: boolean()
  def alpha?(char) when char >= 65 and char <= 90, do: true
  def alpha?(char) when char >= 97 and char <= 122, do: true
  def alpha?(_), do: false

  @doc """
  Returns true if the passed character is a closing comment character.
  """
  @spec closing_comment_char?(char()) :: boolean()
  def closing_comment_char?(125), do: true
  def closing_comment_char?(_), do: false

  @doc """
  Returns true if the passed character is a digit.
  """
  @spec digit?(char()) :: boolean()
  def digit?(char) when char >= 48 and char <= 57, do: true
  def digit?(_), do: false

  @doc """
  Returns true if the passed character is acceptable within an identifier.
  """
  @spec id_char?(char()) :: boolean()
  def id_char?(char), do: first_id_char?(char) || digit?(char)

  @doc """
  Returns true if the passed character is acceptable as the first character of
  an identifier.
  """
  @spec first_id_char?(char()) :: boolean()
  def first_id_char?(char), do: alpha?(char) || char == hd('_')

  @doc """
  Returns true if the passed character is an opening comment character.
  """
  @spec opening_comment_char?(char()) :: boolean()
  def opening_comment_char?(123), do: true
  def opening_comment_char?(_), do: false

  @doc """
  Returns true if the passed character is whitespace.
  """
  @spec whitespace?(char()) :: boolean()
  def whitespace?(10), do: true
  def whitespace?(32), do: true
  def whitespace?(_), do: false

  # Always returns true.
  @spec any_char(any()) :: true
  defp any_char(_), do: true

  @spec log_err(t(), Error.t()) :: :ok

  defp log_err(%__MODULE__{log_to: nil}, _), do: :ok

  defp log_err(
         %__MODULE__{entire_code: code, log_to: device},
         err = %Error{line_no: line_no, col_no: col_no}
       ) do
    line =
      code
      |> String.split("\n")
      |> Enum.at(line_no - 1)

    indicator = String.duplicate(" ", col_no - 1) <> "^"

    IO.puts(device, Error.message(err) <> ":\n")
    IO.puts(device, line)
    IO.puts(device, indicator)
  end
end
