defmodule Pascal.Runner do
  @moduledoc """
  Provides functions for running pascal code.
  """

  alias Pascal.Enrichment
  alias Pascal.Interpreter
  alias Pascal.Lexer
  alias Pascal.Parser
  alias Pascal.SemanticAnalyzer

  @type state :: map()

  @doc """
  Runs the passed pascal code and returns the final state.
  """
  @spec run(String.t(), keyword()) :: :ok
  def run(code, opts \\ []) do
    with {:ok, tokens} <- Lexer.tokenize(code),
         {:ok, ast} <- Parser.parse(tokens),
         :ok <- SemanticAnalyzer.analyze(ast) do
      ast
      |> Enrichment.enrich!()
      |> Interpreter.exec!(opts)
    end
  end
end
