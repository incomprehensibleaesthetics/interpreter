defmodule Pascal.Symbol.BuiltInTypeSymbol do
  @moduledoc """
  A symbol representing a built in type.
  """
  defstruct [:name]

  @type t :: %__MODULE__{}

  @doc """
  Returns a new BuiltInTypeSymbol struct.
  """
  @spec new(String.t()) :: t()
  def new(name) do
    %__MODULE__{name: name}
  end
end
