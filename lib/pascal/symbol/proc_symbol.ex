defmodule Pascal.Symbol.ProcSymbol do
  @moduledoc """
  A symbol representing a procedure.
  """
  defstruct [:name, :params, :block]

  alias Pascal.Ast.Block
  alias Pascal.Symbol.ParamSymbol

  @type t :: %__MODULE__{}

  @doc """
  Returns a new ProcSymbol struct.
  """
  @spec new(String.t(), list(ParamSymbol.t()), Block.t()) :: t()
  def new(name, params, block) do
    %__MODULE__{
      name: name,
      params: params,
      block: block
    }
  end
end
