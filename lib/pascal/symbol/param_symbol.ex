defmodule Pascal.Symbol.ParamSymbol do
  @moduledoc """
  A symbol representing a procedure parameter.
  """
  defstruct [:name, :type]

  alias Pascal.Symbol.BuiltInTypeSymbol

  @type t :: %__MODULE__{}

  @doc """
  Returns a new ParamSymbol struct.
  """
  @spec new(String.t(), BuiltInTypeSymbol.t()) :: t()
  def new(name, type) do
    %__MODULE__{name: name, type: type}
  end
end
