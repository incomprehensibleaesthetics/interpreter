defmodule Pascal.Symbol.VarSymbol do
  @moduledoc """
  A symbol representing a variable.
  """
  defstruct [:name, :type]

  alias Pascal.Symbol.BuiltInTypeSymbol

  @type t :: %__MODULE__{}

  @doc """
  Returns a new VarSymbol struct.
  """
  @spec new(String.t(), BuiltInTypeSymbol.t()) :: t()
  def new(name, type) do
    %__MODULE__{name: name, type: type}
  end
end
