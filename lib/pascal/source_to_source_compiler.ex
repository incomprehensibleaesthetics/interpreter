defmodule Pascal.SourceToSourceCompiler do
  @moduledoc """
  Provides functions for generating pascal code from an ast.
  """

  @default_indent_size 2

  alias Pascal.Ast
  alias Ast.Assign
  alias Ast.BinOp
  alias Ast.Block
  alias Ast.Compound
  alias Ast.NoOp
  alias Ast.Num
  alias Ast.Param
  alias Ast.ProcDecl
  alias Ast.Program
  alias Ast.Type
  alias Ast.UnaryOp
  alias Ast.Var
  alias Ast.VarDecl

  alias GenLexer.Token
  alias Pascal.ScopedSymbolTable

  alias Pascal.Symbol
  alias Symbol.BuiltInTypeSymbol
  alias Symbol.ProcSymbol
  alias Symbol.VarSymbol

  @typedoc """
  The internal state, including the current scope.
  """
  @type state_t :: %{
          current_scope: ScopedSymbolTable.t() | nil,
          indent_level: integer(),
          code: iolist()
        }

  @doc """
  Generates pascal code for the passed abstract syntax tree.
  """
  @spec compile(Ast.t()) :: {:ok, String.t()} | {:error, any()}
  def compile(ast) do
    initial_state = %{
      current_scope: nil,
      code: [],
      indent_level: 0
    }

    with {:ok, %{code: code}} <- visit(ast, initial_state),
         do: {:ok, to_string(code)}
  end

  defp add_built_in_types(scope) do
    scope = ScopedSymbolTable.define(scope, BuiltInTypeSymbol.new("INTEGER"))
    ScopedSymbolTable.define(scope, BuiltInTypeSymbol.new("REAL"))
  end

  @doc """
  Visits the passed ast node and updates the state accordingly.

  Returns an error if a semantic error is encountered.
  """
  @spec visit(Ast.t(), state_t()) :: {:ok, state_t()} | {:error, any()}

  def visit(%Assign{left: left, right: right}, state) do
    with state <- append_to_code(state, "", indent?: true, newline?: false),
         {:ok, state} <- visit(left, state),
         state <- append_to_code(state, " := ", indent?: false, newline?: false),
         {:ok, state} <- visit(right, state),
         state <- append_to_code(state, ";", indent?: false),
         do: {:ok, state}
  end

  def visit(%BinOp{left: left, op: %Token{str: op}, right: right}, state) do
    with {:ok, state} <- visit(left, state),
         state <- append_to_code(state, " #{op} ", indent?: false, newline?: false),
         do: visit(right, state)
  end

  def visit(%Block{declarations: declarations, compound: compound}, state) do
    with state <- increase_indent_level(state),
         {:ok, state} <- visit_nodes(declarations, state),
         state <- decrease_indent_level(state),
         do: visit(compound, state)
  end

  # Handle this special case for prettier formatting.
  def visit(%Compound{children: [%NoOp{}]}, state) do
    state =
      state
      |> append_newline_to_code()
      |> append_to_code("begin")
      |> append_newline_to_code()
      |> append_to_code("end", newline?: false)

    {:ok, state}
  end

  def visit(%Compound{children: children}, state) do
    state =
      state
      |> append_newline_to_code()
      |> append_to_code("begin")
      |> increase_indent_level()

    with {:ok, state} <- visit_nodes(children, state) do
      state =
        state
        |> decrease_indent_level()
        |> append_to_code("end", newline?: false)

      {:ok, state}
    end
  end

  def visit(%NoOp{}, state), do: {:ok, state}

  def visit(%Num{}, state), do: {:ok, state}

  def visit(
        %Param{name_token: %Token{str: name}, type: %Type{token: %Token{str: type}}},
        input_state = %{current_scope: scope}
      ) do
    with {:ok, type_symbol} <- ScopedSymbolTable.lookup(scope, type) do
      %ScopedSymbolTable{scope_level: scope_level} = scope
      var_symbol = VarSymbol.new(name, type_symbol)
      updated_scope = ScopedSymbolTable.define(scope, var_symbol)

      type = String.upcase(type)

      output_state =
        %{input_state | current_scope: updated_scope}
        |> append_to_code("#{name}#{scope_level} : #{type}", indent?: false, newline?: false)

      {:ok, output_state}
    end
  end

  def visit(
        %ProcDecl{name_token: %Token{str: name}, params: params, block: block},
        input_state = %{current_scope: input_outer_scope}
      ) do
    %ScopedSymbolTable{scope_level: outer_scope_level} = input_outer_scope

    # Add a symbol for this procedure to the current scope.
    proc_symbol = ProcSymbol.new(name, params, block)
    updated_outer_scope = ScopedSymbolTable.define(input_outer_scope, proc_symbol)

    proc_scope =
      ScopedSymbolTable.new(name, outer_scope_level + 1)
      |> Map.put(:enclosing_scope, updated_outer_scope)

    proc_input_state =
      input_state
      |> Map.put(:current_scope, proc_scope)
      |> append_to_code("procedure #{name}#{outer_scope_level}(", newline?: false)

    # Visit the parameters, then continue traversing the tree.
    with {:ok, state} <- visit_nodes(params, proc_input_state),
         state <- append_to_code(state, ");", indent?: false),
         {:ok, state} <- visit(block, state),
         state <- append_to_code(state, "; {END OF #{name}}", indent?: false),
         state <- %{state | current_scope: updated_outer_scope},
         do: {:ok, state}
  end

  def visit(%Program{name: %Token{str: name}, block: block}, state) do
    global_scope =
      ScopedSymbolTable.new("global", 1)
      |> add_built_in_types()

    state =
      state
      |> Map.put(:current_scope, global_scope)
      |> append_to_code("program #{name}0;")

    # Continue traversing the ast.
    with {:ok, state} <- visit(block, state),
         state <- append_to_code(state, ". {END OF #{name}}", indent?: false),
         do: {:ok, state}
  end

  def visit(%UnaryOp{val: val}, state), do: visit(val, state)

  def visit(%Var{token: %Token{str: name}}, state = %{current_scope: scope}) do
    with {:ok, %{scope_level: scope_level, symbol: sym}} <-
           ScopedSymbolTable.lookup_meta(scope, name) do
      %VarSymbol{type: %BuiltInTypeSymbol{name: type}} = sym

      state =
        state
        |> append_to_code("<#{name}#{scope_level}:#{type}>", indent?: false, newline?: false)

      {:ok, state}
    end
  end

  def visit(
        %VarDecl{name: %Token{str: name}, type: %Type{token: %Token{str: type}}},
        state = %{current_scope: scope}
      ) do
    with {:ok, type_symbol} <- ScopedSymbolTable.lookup(scope, type) do
      %ScopedSymbolTable{scope_level: scope_level} = scope
      var_symbol = VarSymbol.new(name, type_symbol)
      updated_scope = ScopedSymbolTable.define(scope, var_symbol)

      type = String.upcase(type)

      updated_state =
        state
        |> Map.put(:current_scope, updated_scope)
        |> append_to_code("var #{name}#{scope_level} : #{type};")

      {:ok, updated_state}
    end
  end

  # Visits each node from the passed list, updating the state.
  #
  # Returns the new state or the first encountered error.
  @spec visit_nodes([Ast.t()], state_t()) :: {:ok, state_t()} | {:error, any()}
  defp visit_nodes(nodes, input_state) do
    Enum.reduce_while(nodes, {:ok, input_state}, fn
      node, {:ok, state} -> {:cont, visit(node, state)}
      _, {:error, _} = err -> {:halt, err}
    end)
  end

  @spec append_to_code(state_t(), String.t() | iolist(), keyword()) :: state_t()
  defp append_to_code(state = %{indent_level: indent_level}, new_code, options \\ []) do
    prefix =
      case Keyword.fetch(options, :indent?) do
        {:ok, false} -> ""
        _ -> String.duplicate(" ", indent_level)
      end

    suffix =
      case Keyword.fetch(options, :newline?) do
        {:ok, false} -> ""
        _ -> "\n"
      end

    Map.update(state, :code, [], fn c -> [c, prefix, new_code, suffix] end)
  end

  @spec append_newline_to_code(state_t()) :: state_t()
  defp append_newline_to_code(state),
    do: append_to_code(state, "", indent?: false, newline?: true)

  @spec increase_indent_level(state_t()) :: state_t()
  defp increase_indent_level(state) do
    Map.update(state, :indent_level, 0, fn lvl -> lvl + @default_indent_size end)
  end

  @spec decrease_indent_level(state_t()) :: state_t()
  defp decrease_indent_level(state) do
    Map.update(state, :indent_level, @default_indent_size, fn lvl ->
      lvl - @default_indent_size
    end)
  end
end
