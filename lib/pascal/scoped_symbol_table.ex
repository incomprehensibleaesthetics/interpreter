defmodule Pascal.ScopedSymbolTable do
  @moduledoc """
  Keeps track of various symbols in the abstract syntax tree.
  """
  defstruct [:symbols, :scope_name, :scope_level, :enclosing_scope]

  alias Pascal.Symbol

  @type t :: %__MODULE__{}

  @typedoc """
  Metadata for a symbol.
  """
  @type meta_t :: %{
          scope_name: String.t(),
          scope_level: integer(),
          symbol: Symbol.t()
        }

  @doc """
  Returns a new ScopedSymbolTable struct.
  """
  @spec new(String.t(), integer(), t() | nil) :: t()
  def new(scope_name, scope_level, enclosing_scope \\ nil) do
    %__MODULE__{
      symbols: %{},
      scope_name: scope_name,
      scope_level: scope_level,
      enclosing_scope: enclosing_scope
    }
  end

  @doc """
  Adds a symbol to the table.
  """
  @spec define(t(), Symbol.t()) :: t()
  def define(table, sym) do
    # IO.puts("Define: #{sym.name}")
    canonical_sym_name = canonical_name(sym.name)
    updated_symbols = Map.put(table.symbols, canonical_sym_name, sym)
    %__MODULE__{table | symbols: updated_symbols}
  end

  @doc """
  Looks up a previously defined symbol and returns it.
  """
  @spec lookup(t(), String.t(), boolean()) ::
          {:ok, Symbol.t()} | {:error, {:symbol_not_found, String.t()}}
  def lookup(
        table = %__MODULE__{scope_name: _scope_name, enclosing_scope: outer_table},
        sym_name,
        check_enclosing_scope \\ true
      ) do
    # IO.puts("Lookup: #{sym_name} (scope: #{scope_name})")

    canonical_sym_name = canonical_name(sym_name)

    case Map.fetch(table.symbols, canonical_sym_name) do
      :error ->
        if outer_table && check_enclosing_scope do
          lookup(outer_table, sym_name)
        else
          {:error, {:symbol_not_found, sym_name}}
        end

      r ->
        r
    end
  end

  @doc """
  Looks up metadata for a previously defined symbol and returns it.
  """
  @spec lookup_meta(t(), String.t(), boolean()) ::
          {:ok, meta_t()} | {:error, {:symbol_not_found, String.t()}}
  def lookup_meta(
        table = %__MODULE__{enclosing_scope: outer_table},
        sym_name,
        check_enclosing_scope \\ true
      ) do
    canonical_sym_name = canonical_name(sym_name)

    case Map.fetch(table.symbols, canonical_sym_name) do
      :error ->
        if outer_table && check_enclosing_scope do
          lookup_meta(outer_table, sym_name)
        else
          {:error, {:symbol_not_found, sym_name}}
        end

      {:ok, sym} ->
        {:ok, %{scope_name: table.scope_name, scope_level: table.scope_level, symbol: sym}}
    end
  end

  @spec canonical_name(String.t()) :: String.t()
  defp canonical_name(name), do: String.downcase(name)
end

defimpl String.Chars, for: Pascal.ScopedSymbolTable do
  def to_string(%Pascal.ScopedSymbolTable{symbols: symbols, scope_name: name, scope_level: level}) do
    s = """
    SCOPE (SCOPED SYMBOL TABLE)
    ===========================
    Scope name     : #{name}
    Scope level    : #{level}
    Scope (Scoped Symbol Table) contents
    ------------------------------------
    """

    Enum.reduce(symbols, s, fn {name, sym}, str ->
      "#{str}#{String.pad_leading(name, 7)}: #{inspect(sym)}\n"
    end)
  end
end
