defmodule GenLexer.Token do
  @moduledoc """
  A token.
  """
  defstruct [:name, :str, :line_no, :col_no]

  @type t :: %__MODULE__{}

  @doc """
  Returns a new Token struct.
  """
  @spec new(atom(), String.t(), options :: [pos: {integer(), integer()}]) :: t()
  def new(name, str, options \\ []) do
    {line_no, col_no} = Keyword.get(options, :pos, {nil, nil})

    %__MODULE__{
      name: name,
      str: str,
      line_no: line_no,
      col_no: col_no
    }
  end

  @doc """
  Updates the position (line and column) of the token.
  """
  @spec set_pos(t(), integer(), integer()) :: t()
  def set_pos(token, line_no, col_no) do
    %__MODULE__{token | line_no: line_no, col_no: col_no}
  end
end
