defmodule GenLexer.Macros do
  alias GenLexer.Token

  @doc """
  Defines the necessary function clauses for handling the specified symbol (one
  or more characters, that needn't be terminated by an end of word character).

  More specifically: Defines two `eat_token(chars, <token_name>)` function
  clauses.
  """
  defmacro def_symbol(token_name, char_seq) do
    quote do
      defp eat_token(unquote(char_seq) ++ _, unquote(token_name)) do
        token = Token.new(unquote(token_name), to_string(unquote(char_seq)))
        {:ok, token}
      end

      defp eat_token(chars, unquote(token_name)), do: :error
    end
  end

  @doc """
  Defines the necessary function clauses for handling the specified reserved keyword.

  More specifically: Defines an `eat_token(chars, <token_name>)` function clause.
  """
  defmacro def_keyword(token_name, char_seq) do
    lower_str =
      char_seq
      |> to_string()
      |> String.downcase()

    quote do
      defp eat_token(chars, unquote(token_name)) do
        with {:ok, %Token{str: word}} <- eat_token(chars, :potential_id) do
          case String.downcase(word) do
            unquote(lower_str) -> {:ok, Token.new(unquote(token_name), word)}
            _ -> :error
          end
        end
      end
    end
  end

  @doc """
  Defines the necessary function clauses for handling a dynamic sequence of characters.

  More specifically: Defines an `eat_token(chars, <token_name>)` function
  clause and a `leading_<token_name>` function.
  """
  defmacro def_char_seq(token_name, char_checker, options \\ []) do
    first_char_checker = Keyword.get(options, :first_char, char_checker)

    # TODO: Find a clearer way to express this.
    cond_block =
      case Keyword.fetch(options, :last_char) do
        {:ok, last_char_checker} ->
          quote do
            cond do
              unquote(last_char_checker)(char) -> {:halt, [char | acc]}
              unquote(char_checker)(char) -> {:cont, [char | acc]}
              true -> {:halt, acc}
            end
          end

        :error ->
          quote do
            cond do
              unquote(char_checker)(char) -> {:cont, [char | acc]}
              true -> {:halt, acc}
            end
          end
      end

    leading_chars_fun = :"leading_#{token_name}"

    quote do
      defp eat_token(input_chars, unquote(token_name)) do
        chars = unquote(leading_chars_fun)(input_chars)

        if length(chars) == 0 do
          :error
        else
          str = chars |> to_string()
          token = Token.new(unquote(token_name), str)
          {:ok, token}
        end
      end

      @spec unquote(leading_chars_fun)(charlist()) :: charlist()

      defp unquote(leading_chars_fun)(''), do: ''

      defp unquote(leading_chars_fun)(chars) do
        if chars |> hd() |> unquote(first_char_checker)() do
          Enum.reduce_while(chars, '', fn char, acc -> unquote(cond_block) end)
          |> Enum.reverse()
        else
          ''
        end
      end
    end
  end
end
