defmodule GenLexer.Error do
  @moduledoc """
  A lexer error.
  """
  defexception [:name, :line_no, :col_no]

  @type t :: %__MODULE__{}

  @doc """
  Returns a new GenLexer.Error struct.
  """
  @spec new(atom(), integer(), integer()) :: t()
  def new(name, line_no, col_no) do
    %__MODULE__{
      name: name,
      line_no: line_no,
      col_no: col_no
    }
  end

  def message(err) do
    case err.name do
      :invalid_token -> "invalid token on line #{err.line_no} at column #{err.col_no}"
      name -> "unknown lexer error '#{name}' on line #{err.line_no} at column #{err.col_no}"
    end
  end
end
