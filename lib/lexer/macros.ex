defmodule Lexer.Macros do
  @doc """
  Defines the necessary function clauses for handling the specified keyword.
  """
  defmacro def_keyword(keyword, char_seq) do
    char_seq_lower_str =
      char_seq
      |> to_string()
      |> String.downcase()

    quote do
      def eat(chars, unquote(keyword), false) do
        case eat_keyword_or_id(chars) do
          {:ok, {{unquote(keyword), _}, _}} = ret_val -> ret_val
          {:error, _} = err -> err
          _ -> {:error, "expected #{unquote(char_seq)} but got '#{chars}'"}
        end
      end

      defp keyword_token(unquote(char_seq_lower_str), false) do
        {unquote(keyword), unquote(char_seq |> to_string())}
      end
    end
  end

  @doc """
  Defines the necessary function clauses for handling the specified symbol (one
  or more characters, that needn't be terminated by an end of word character).
  """
  defmacro def_symbol(name, char_seq) do
    quote do
      def eat(unquote(char_seq) ++ rest, unquote(name), false) do
        {:ok, {{unquote(name), to_string(unquote(char_seq))}, rest}}
      end

      def eat(chars, unquote(name), false) do
        {:error, "expected '#{unquote(char_seq)}' but got '#{chars}'"}
      end
    end
  end
end
