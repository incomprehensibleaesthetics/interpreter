defmodule CallStack do
  @moduledoc """
  A stack of activation records.
  """
  defstruct records: []

  @type t :: %__MODULE__{}

  @doc """
  Returns a new CallStack struct.
  """
  @spec new() :: t()
  def new, do: %__MODULE__{}

  @doc """
  Pushes an existing record onto the stack.
  """
  @spec push(t(), ActivationRecord.t()) :: t()
  def push(stack, record), do: Map.put(stack, :records, [record | stack.records])

  @doc """
  Creates a new record and pushes it onto the stack.

  The new record will have a nesting_level that is one higher than the previous
  record from the stack (default: 1).
  """
  @spec push_new(t(), String.t(), ActivationRecord.record_type()) :: t()
  def push_new(stack, record_name, record_type) do
    # Determine the current nesting level.
    {record, _} = pop(stack)

    current_nesting_level =
      case record do
        %ActivationRecord{} -> record.nesting_level
        nil -> 0
      end

    # Create a new record one level deeper than the last.
    record = ActivationRecord.new(record_name, record_type, current_nesting_level + 1)

    # Push the new record to the stack.
    push(stack, record)
  end

  @doc """
  Pops the top record from the stack.

  Returns nil as the first element of the return tuple if the stack is empty.
  """
  @spec pop(t()) :: {ActivationRecord.t() | nil, t()}
  def pop(stack) do
    [hd_rec | tl_recs] =
      if Enum.empty?(stack.records) do
        [nil]
      else
        stack.records
      end

    {hd_rec, Map.put(stack, :records, tl_recs)}
  end

  @doc """
  Updates the record at the top of the call stack.
  """
  @spec update_hd(t(), (ActivationRecord.t() -> ActivationRecord.t())) :: t()
  def update_hd(stack, fun) do
    {record, stack} = pop(stack)
    updated_record = fun.(record)
    push(stack, updated_record)
  end
end

defimpl String.Chars, for: CallStack do
  def to_string(stack) do
    record_strings = stack.records |> Enum.map(&Kernel.to_string/1)

    ["CALL STACK" | record_strings]
    |> Enum.join("\n")
  end
end
