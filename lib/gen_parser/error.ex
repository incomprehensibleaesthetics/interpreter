defmodule GenParser.Error do
  @moduledoc """
  A parser error.
  """
  defexception [:name, :bad_token, :unmet_expectation]

  @type t :: %__MODULE__{}

  @doc """
  Returns a new GenParser.Error struct.
  """
  @spec new(atom(), keyword()) :: t()
  def new(name, options \\ []) do
    bad_token = Keyword.get(options, :bad_token)
    unmet_expectation = Keyword.get(options, :unmet_expectation)

    %__MODULE__{
      name: name,
      bad_token: bad_token,
      unmet_expectation: unmet_expectation
    }
  end

  def message(err) do
    case err.name do
      :missing_eof -> "the final token must be an :eof token"
      :bad_syntax -> "expected #{err.unmet_expectation} but got '#{err.bad_token.str}' instead"
      :trailing_code -> "expected end of code but got '#{err.bad_token.str}' instead"
      :unexpected_eof -> "expected #{err.unmet_expectation} but there are no more tokens"
      _ -> "unknown parser error '#{err.name}'"
    end
  end
end
