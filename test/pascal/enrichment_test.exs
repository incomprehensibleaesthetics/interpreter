defmodule Pascal.EnrichmentTest do
  use ExUnit.Case
  doctest Pascal.Enrichment

  alias Pascal.Enrichment, as: Subject

  alias Pascal.Ast
  alias Ast.Block
  alias Ast.Compound
  alias Ast.ProcCall
  alias Ast.ProcDecl

  alias Pascal.Lexer
  alias Pascal.Parser

  alias Pascal.Symbol
  alias Symbol.BuiltInTypeSymbol
  alias Symbol.ParamSymbol
  alias Symbol.ProcSymbol

  describe "enrich!/1" do
    test "returns the input ast unchanged if no procedures were used" do
      # Setup
      input_ast =
        generate_ast("""
        program test;
          var x, y : integer;

        begin
          x := y + 1
        end.
        """)

      # Execution
      ret_val = Subject.enrich!(input_ast)

      # Assertion
      assert input_ast == ret_val
    end

    test "returns the input ast but with each procedure call node pointing to its respective symbol" do
      # Setup
      input_ast =
        generate_ast("""
        program test;

        procedure add(a, b: integer);
        begin
          { ... }
        end;

        procedure subtract(x, y: integer);
        begin
          { ... }
        end;

        begin
          add(1, 2);
          subtract(4, 3)
        end.
        """)

      # Execution
      ret_val = Subject.enrich!(input_ast)

      # Assertion

      [add_call, subtract_call] = input_ast.block.compound.children

      [add_body, subtract_body] =
        input_ast.block.declarations
        |> Enum.map(&Map.fetch!(&1, :block))

      add_params = [
        ParamSymbol.new("a", BuiltInTypeSymbol.new("integer")),
        ParamSymbol.new("b", BuiltInTypeSymbol.new("integer"))
      ]

      add_call = %ProcCall{add_call | symbol: ProcSymbol.new("add", add_params, add_body)}

      subtract_params = [
        ParamSymbol.new("x", BuiltInTypeSymbol.new("integer")),
        ParamSymbol.new("y", BuiltInTypeSymbol.new("integer"))
      ]

      subtract_call = %ProcCall{
        subtract_call
        | symbol: ProcSymbol.new("subtract", subtract_params, subtract_body)
      }

      expected_ast = put_in(input_ast.block.compound.children, [add_call, subtract_call])
      assert expected_ast == ret_val
    end

    test "handles nested procedures as expected" do
      # Setup
      input_ast =
        generate_ast("""
        program test;

        procedure transform(x, y, z: integer);

          procedure rotate(x, y: integer);
          begin
            { ... }
          end;

        begin { transform }
          rotate(x, y)
        end;

        begin { test }
          transform(1, 2, 3)
        end.
        """)

      # Execution
      ret_val = Subject.enrich!(input_ast)

      # Assertion

      [input_transform_call] = input_ast.block.compound.children

      [%ProcDecl{block: %Block{compound: %Compound{children: [input_rotate_call]}}}] =
        input_ast.block.declarations

      [%ProcDecl{block: input_transform_body}] = input_ast.block.declarations

      [%ProcDecl{block: rotate_body}] = input_transform_body.declarations

      rotate_params = [
        ParamSymbol.new("x", BuiltInTypeSymbol.new("integer")),
        ParamSymbol.new("y", BuiltInTypeSymbol.new("integer"))
      ]

      updated_rotate_call = %ProcCall{
        input_rotate_call
        | symbol: ProcSymbol.new("rotate", rotate_params, rotate_body)
      }

      transform_params = [
        ParamSymbol.new("x", BuiltInTypeSymbol.new("integer")),
        ParamSymbol.new("y", BuiltInTypeSymbol.new("integer")),
        ParamSymbol.new("z", BuiltInTypeSymbol.new("integer"))
      ]

      updated_transform_body =
        put_in(input_transform_body.compound.children, [updated_rotate_call])

      updated_transform_call = %ProcCall{
        input_transform_call
        | symbol: ProcSymbol.new("transform", transform_params, updated_transform_body)
      }

      expected_ast =
        update_in(input_ast.block.declarations, fn [transform_decl] ->
          [Map.put(transform_decl, :block, updated_transform_body)]
        end)

      expected_ast = put_in(expected_ast.block.compound.children, [updated_transform_call])

      assert expected_ast == ret_val
    end
  end

  defp generate_ast(code) do
    code
    |> Lexer.tokenize!()
    |> Parser.parse!()
  end
end
