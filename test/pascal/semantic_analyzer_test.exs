defmodule Pascal.SemanticAnalyzerTest do
  use ExUnit.Case
  doctest Pascal.SemanticAnalyzer

  alias Pascal.SemanticAnalyzer, as: Subject
  alias Subject.Error

  alias Pascal.Ast
  alias Ast.Assign
  alias Ast.Block
  alias Ast.Compound
  alias Ast.Num
  alias Ast.ProcDecl
  alias Ast.Program
  alias Ast.Var

  alias GenLexer.Token

  alias Pascal.Lexer
  alias Pascal.Parser
  alias Pascal.ScopedSymbolTable
  alias Pascal.Symbol.ProcSymbol

  describe "analyze/1" do
    test "returns :ok if the specified ast is semantically correct" do
      input_ast =
        generate_ast("""
        program SymTab3;
          var x, y : integer;

        begin

        end.
        """)

      assert Subject.analyze(input_ast) == :ok
    end

    test "returns an undeclared_var error if the specified ast references an undeclared variable" do
      input_ast =
        generate_ast("""
        program SymTab5;
          var x : integer;

        begin
          x := y;
        end.
        """)

      assert {:error, %Error{name: :undeclared_var, token: %Token{str: "y"}}} =
               Subject.analyze(input_ast)
    end

    test "returns a duplicate_var_declaration error if the specified ast declares a variable twice" do
      input_ast =
        generate_ast("""
        program SymTab6;
          var x, y : integer;
              y : real;
        begin
           x := x + y;
        end.
        """)

      assert {:error,
              %Error{name: :duplicate_var_declaration, token: %Token{str: "y", line_no: 3}}} =
               Subject.analyze(input_ast)
    end

    test "returns an undeclared_proc error if the specified ast references an undeclared procedure" do
      input_ast =
        generate_ast("""
        program proc_test;

        begin
          add(1, 2)
        end.
        """)

      assert {:error, %Error{name: :undeclared_proc, token: %Token{str: "add"}}} =
               Subject.analyze(input_ast)
    end

    test "returns an invalid_args error if a procedure is called with less arguments than were specified in the procedure declaration" do
      input_ast =
        generate_ast("""
        program args_test;
          procedure add(a, b: integer);
            var result: integer;

          begin { add }
            result := a + b
          end; { add }
            
        begin { args_test }
           add(1)
        end. { args_test }
        """)

      assert {:error, %Error{name: :invalid_args, token: %Token{str: "add", line_no: 10}}} =
               Subject.analyze(input_ast)
    end

    test "returns an invalid_args error if a procedure is called with more arguments than were specified in the procedure declaration" do
      input_ast =
        generate_ast("""
        program args_test;
          procedure add(a, b: integer);
            var result: integer;

          begin { add }
            result := a + b
          end; { add }
            
        begin { args_test }
           add(1, 2, 3)
        end. { args_test }
        """)

      assert {:error, %Error{name: :invalid_args, token: %Token{str: "add", line_no: 10}}} =
               Subject.analyze(input_ast)
    end

    test "understands procedure parameters" do
      input_ast =
        generate_ast("""
        program Main;
          procedure Alpha(a: integer);
          begin
            a := 42;
          end;

        begin { Main }
        end.  { Main }
        """)

      assert Subject.analyze(input_ast) == :ok
    end

    test "allows access to variables of an outer scope" do
      input_ast =
        generate_ast("""
        program Main;
          var x: integer;

          procedure Alpha(a: integer);
          begin
            a := 42 + x;
          end;

        begin { Main }
        end.  { Main }
        """)

      assert Subject.analyze(input_ast) == :ok
    end

    test "frees an inner scope when necessary" do
      input_ast =
        generate_ast("""
        program Main;
          procedure Alpha(a: integer);
          begin
            a := 42;
          end;

        begin { Main }
          a := 0
        end.  { Main }
        """)

      assert {:error, %Error{name: :undeclared_var, token: %Token{str: "a"}}} =
               Subject.analyze(input_ast)
    end

    test "ignores case for variables" do
      input_ast =
        generate_ast("""
        program Main;
          var aLiCE: integer;
        begin
          alice := 1;
          ALICE := 2;
        end.
        """)

      assert Subject.analyze(input_ast) == :ok
    end
  end

  describe "visit(%Assign{}, _)" do
    test "returns an error if the variable being assigned is not in the scope" do
      ast = %Assign{
        left: %Var{token: %Token{name: :id, str: "foo"}},
        op: %Token{name: :assign, str: ":="},
        right: %Num{token: %Token{name: :integer_const, str: "42"}}
      }

      scope = ScopedSymbolTable.new("test", 1)

      assert {:error, %Error{name: :undeclared_var, token: %Token{str: "foo"}}} =
               Subject.visit(ast, %{current_scope: scope})
    end
  end

  describe "visit(%ProcDecl{}, _)" do
    test "updates the current scope appropriately" do
      ast = %ProcDecl{
        name_token: %Token{name: :id, str: "foo"},
        params: [],
        block: %Block{
          declarations: [],
          compound: %Compound{children: []}
        }
      }

      input_scope = ScopedSymbolTable.new("outer", 3)

      assert {:ok,
              %{
                current_scope: %ScopedSymbolTable{
                  symbols: %{"foo" => %ProcSymbol{name: "foo"}},
                  scope_name: "outer",
                  scope_level: 3,
                  enclosing_scope: nil
                }
              }} = Subject.visit(ast, %{current_scope: input_scope})
    end

    test "evaluates the inner block" do
      ast = %ProcDecl{
        name_token: %Token{name: :id, str: "test"},
        params: [],
        block: %Block{
          declarations: [],
          compound: %Compound{
            children: [
              %Assign{
                left: %Var{token: %Token{name: :id, str: "foo"}},
                op: %Token{name: :assign, str: ":="},
                right: %Num{token: %Token{name: :integer_const, str: "42"}}
              }
            ]
          }
        }
      }

      scope = ScopedSymbolTable.new("test", 1)

      assert {:error, %Error{name: :undeclared_var, token: %Token{str: "foo"}}} =
               Subject.visit(ast, %{current_scope: scope})
    end
  end

  describe "visit(%Program{}, _)" do
    test "sets the current scope appropriately" do
      ast = %Program{
        name: %Token{name: :id, str: "test"},
        block: %Block{
          declarations: [],
          compound: %Compound{children: []}
        }
      }

      outer_scope = ScopedSymbolTable.new("outer", 9000)

      assert {:ok,
              %{
                current_scope: %ScopedSymbolTable{
                  symbols: _,
                  scope_name: "global",
                  scope_level: 9001,
                  enclosing_scope: outer_scope
                }
              }} = Subject.visit(ast, %{current_scope: outer_scope})
    end
  end

  describe "visit(%Var{}, _)" do
    test "returns an error if the referenced variable is not in the scope" do
      ast = %Var{token: %Token{name: :id, str: "foo"}}
      scope = ScopedSymbolTable.new("test", 1)

      assert {:error, %Error{name: :undeclared_var, token: %Token{str: "foo"}}} =
               Subject.visit(ast, %{current_scope: scope})
    end
  end

  defp generate_ast(code) do
    code
    |> Lexer.tokenize!()
    |> Parser.parse!()
  end
end
