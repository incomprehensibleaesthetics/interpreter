defmodule Pascal.SourceToSourceCompilerTest do
  use ExUnit.Case
  doctest Pascal.SourceToSourceCompiler

  alias Pascal.SourceToSourceCompiler, as: Subject

  alias Pascal.Lexer
  alias Pascal.Parser

  describe "compile" do
    test "adds subscripts" do
      # Setup

      code = """
      program Main;
        var b, x, y : real;
        var z : integer;

        procedure AlphaA(a : integer);
          var b : integer;

          procedure Beta(c : integer);
            var y : integer;

            procedure Gamma(c : integer);
              var x : integer;
            begin { Gamma }
              x := a + b + c + x + y + z;
            end;  { Gamma }

          begin { Beta }

          end;  { Beta }

        begin { AlphaA }

        end;  { AlphaA }

        procedure AlphaB(a : integer);
          var c : real;
        begin { AlphaB }
          c := a + b;
        end;  { AlphaB }

      begin { Main }
      end.  { Main }
      """

      input =
        code
        |> Lexer.tokenize!()
        |> Parser.parse!()

      # Execution
      {:ok, actual_output} = Subject.compile(input)

      # Assertion

      expected_output = """
      program Main0;
        var b1 : REAL;
        var x1 : REAL;
        var y1 : REAL;
        var z1 : INTEGER;
        procedure AlphaA1(a2 : INTEGER);
          var b2 : INTEGER;
          procedure Beta2(c3 : INTEGER);
            var y3 : INTEGER;
            procedure Gamma3(c4 : INTEGER);
              var x4 : INTEGER;

            begin
              <x4:INTEGER> := <a2:INTEGER> + <b2:INTEGER> + <c4:INTEGER> + <x4:INTEGER> + <y3:INTEGER> + <z1:INTEGER>;
            end; {END OF Gamma}

          begin

          end; {END OF Beta}

        begin

        end; {END OF AlphaA}
        procedure AlphaB1(a2 : INTEGER);
          var c2 : REAL;

        begin
          <c2:REAL> := <a2:INTEGER> + <b1:REAL>;
        end; {END OF AlphaB}

      begin

      end. {END OF Main}
      """

      assert actual_output == expected_output
    end
  end
end
