defmodule Pascal.ScopedSymbolTableTest do
  use ExUnit.Case
  doctest Pascal.ScopedSymbolTable

  alias Pascal.ScopedSymbolTable, as: Subject

  alias Pascal.Symbol.BuiltInTypeSymbol

  describe "define" do
    test "makes the specified symbol available for lookup" do
      # Setup
      subj = Subject.new("test", 1)
      sym = BuiltInTypeSymbol.new("foo")

      # Execution
      subj = Subject.define(subj, sym)

      # Assertion
      assert Subject.lookup(subj, "foo") == {:ok, sym}
    end
  end

  describe "lookup" do
    test "returns a previously defined symbol" do
      # Setup
      subj = Subject.new("test", 1)
      sym = BuiltInTypeSymbol.new("foo")
      subj = Subject.define(subj, sym)

      # Execution / Assertion
      assert Subject.lookup(subj, "foo") == {:ok, sym}
    end

    test "ignores case" do
      # Setup
      subj = Subject.new("test", 1)
      sym = BuiltInTypeSymbol.new("aLiCE")
      subj = Subject.define(subj, sym)

      # Execution / Assertions
      assert Subject.lookup(subj, "alice") == {:ok, sym}
      assert Subject.lookup(subj, "ALICE") == {:ok, sym}
    end

    test "returns an error if the specified symbol was not found" do
      # Setup
      subj = Subject.new("test", 1)
      sym = BuiltInTypeSymbol.new("foo")
      subj = Subject.define(subj, sym)

      # Execution / Assertion
      assert Subject.lookup(subj, "bar") == {:error, {:symbol_not_found, "bar"}}
    end
  end

  describe "lookup_meta" do
    test "returns metadata for a previously defined symbol" do
      # Setup
      subj = Subject.new("test", 42)
      sym = BuiltInTypeSymbol.new("foo")
      subj = Subject.define(subj, sym)

      # Execution
      {:ok, actual_meta} = Subject.lookup_meta(subj, "foo")

      # Assertion
      assert %{scope_name: "test", scope_level: 42, symbol: sym} = actual_meta
    end

    test "returns an error if the specified symbol was not found" do
      # Setup
      subj = Subject.new("test", 1)
      sym = BuiltInTypeSymbol.new("foo")
      subj = Subject.define(subj, sym)

      # Execution / Assertion
      assert Subject.lookup_meta(subj, "bar") == {:error, {:symbol_not_found, "bar"}}
    end
  end
end
