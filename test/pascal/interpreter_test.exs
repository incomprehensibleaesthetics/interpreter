defmodule Pascal.InterpreterTest do
  use ExUnit.Case
  doctest Pascal.Interpreter

  alias Pascal.Interpreter, as: Subject

  alias Pascal.Ast
  alias Ast.Assign
  alias Ast.BinOp
  alias Ast.Block
  alias Ast.Compound
  alias Ast.NoOp
  alias Ast.Num
  alias Ast.Param
  alias Ast.ProcCall
  alias Ast.ProcDecl
  alias Ast.Program
  alias Ast.Type
  alias Ast.UnaryOp
  alias Ast.Var
  alias Ast.VarDecl

  alias GenLexer.Token

  alias Pascal.Symbol.ProcSymbol

  setup do
    # Create an interpreter with a non-empty call stack.

    record = ActivationRecord.new("test", :program, 42)

    initial_call_stack =
      CallStack.new()
      |> CallStack.push(record)

    subj =
      Subject.new()
      |> Map.put(:call_stack, initial_call_stack)

    %{subj: subj}
  end

  describe "eval!(_, %Assign{})" do
    test "returns nil", %{subj: subj} do
      # Setup

      # foo := 42
      input =
        Assign.new(
          Var.new(Token.new(:id, "foo")),
          Token.new(:assign, ":="),
          Num.new(Token.new(:integer_const, "42"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "sets the value of the respective variable in the call stack", %{subj: subj} do
      # Setup

      # foo := 42
      input =
        Assign.new(
          Var.new(Token.new(:id, "foo")),
          Token.new(:assign, ":="),
          Num.new(Token.new(:integer_const, "42"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      {_, subj} = ret_val
      assert Subject.fetch(subj, "foo") == {:ok, 42}
    end
  end

  describe "eval!(_, %BinOp{})" do
    test "returns the result of an addition", %{subj: subj} do
      # Setup
      input =
        BinOp.new(
          Num.new(Token.new(:integer_const, "1")),
          Token.new(:plus, "+"),
          Num.new(Token.new(:integer_const, "2"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {3, _} = ret_val
    end

    test "returns the result of a subtraction", %{subj: subj} do
      # Setup
      input =
        BinOp.new(
          Num.new(Token.new(:integer_const, "3")),
          Token.new(:minus, "-"),
          Num.new(Token.new(:integer_const, "2"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {1, _} = ret_val
    end

    test "returns the result of a multiplication", %{subj: subj} do
      # Setup
      input =
        BinOp.new(
          Num.new(Token.new(:integer_const, "2")),
          Token.new(:mul, "*"),
          Num.new(Token.new(:integer_const, "3"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {6, _} = ret_val
    end

    test "returns the result of an integer division", %{subj: subj} do
      # Setup
      input =
        BinOp.new(
          Num.new(Token.new(:integer_const, "6")),
          Token.new(:integer_div, "DIV"),
          Num.new(Token.new(:integer_const, "2"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {3, _} = ret_val
    end

    test "returns the result of a float division", %{subj: subj} do
      # Setup
      input =
        BinOp.new(
          Num.new(Token.new(:integer_const, "3")),
          Token.new(:float_div, "/"),
          Num.new(Token.new(:integer_const, "2"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {1.5, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      input =
        BinOp.new(
          Num.new(Token.new(:integer_const, "1")),
          Token.new(:plus, "+"),
          Num.new(Token.new(:integer_const, "2"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  describe "eval!(_, %Block{})" do
    test "returns nil", %{subj: subj} do
      # Setup
      input = Block.new([], Compound.new([]))

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "evaluates the compound statement", %{subj: subj} do
      # Setup
      input =
        Block.new(
          [],
          Compound.new([
            Assign.new(
              Var.new(Token.new(:id, "foo")),
              Token.new(:assign, ":="),
              Num.new(Token.new(:integer_const, "42"))
            )
          ])
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      {_, subj} = ret_val
      assert Subject.fetch(subj, "foo") == {:ok, 42}
    end
  end

  describe "eval!(_, %Compound{})" do
    test "returns nil", %{subj: subj} do
      # Setup
      input = Compound.new([])

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "evaluates each child statement", %{subj: subj} do
      # Setup
      input =
        Compound.new([
          Assign.new(
            Var.new(Token.new(:id, "foo")),
            Token.new(:assign, ":="),
            Num.new(Token.new(:integer_const, "1"))
          ),
          Assign.new(
            Var.new(Token.new(:id, "bar")),
            Token.new(:assign, ":="),
            Num.new(Token.new(:integer_const, "2"))
          )
        ])

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      {_, subj} = ret_val
      assert Subject.fetch(subj, "foo") == {:ok, 1}
      assert Subject.fetch(subj, "bar") == {:ok, 2}
    end
  end

  describe "eval!(_, %NoOp{})" do
    test "returns nil", %{subj: subj} do
      # Setup
      input = NoOp.new()

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      input = NoOp.new()

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  describe "eval!(_, %Num{})" do
    test "returns the value of the input integer", %{subj: subj} do
      # Setup
      input = Num.new(Token.new(:integer_const, "42"))

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {42, _} = ret_val
    end

    test "returns the value of the input real", %{subj: subj} do
      # Setup
      input = Num.new(Token.new(:real_const, "12.345"))

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {12.345, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      input = Num.new(Token.new(:integer_const, "42"))

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  describe "eval!(_, %Param{})" do
    test "returns nil", %{subj: subj} do
      # Setup
      input =
        Param.new(
          Token.new(:id, "foo"),
          Type.new(Token.new(:integer, "INTEGER"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      input =
        Param.new(
          Token.new(:id, "foo"),
          Type.new(Token.new(:integer, "INTEGER"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  describe "eval!(_, %ProcCall{})" do
    test "returns nil", %{subj: subj} do
      # Setup
      sym = ProcSymbol.new("foo", [], Block.new([], Compound.new([])))
      input = ProcCall.new(Token.new(:id, "foo"), [], sym)

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      sym = ProcSymbol.new("foo", [], Block.new([], Compound.new([])))
      input = ProcCall.new(Token.new(:id, "foo"), [], sym)

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  describe "eval!(_, %ProcDecl{})" do
    test "returns nil", %{subj: subj} do
      # Setup
      input =
        ProcDecl.new(
          Token.new(:id, "foo"),
          [],
          Block.new([], Compound.new([]))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      input =
        ProcDecl.new(
          Token.new(:id, "foo"),
          [],
          Block.new([], Compound.new([]))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  describe "eval!(_, %Program{})" do
    test "returns nil", %{subj: subj} do
      # Setup
      input =
        Program.new(
          Token.new(:id, "foo"),
          Block.new([], Compound.new([]))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      input =
        Program.new(
          Token.new(:id, "foo"),
          Block.new([], Compound.new([]))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end

    # TODO: Develop a way to test the call stack changes.
  end

  describe "eval!(_, %Type{})" do
    test "returns nil", %{subj: subj} do
      # Setup
      input = Type.new(Token.new(:integer, "INTEGER"))

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      input = Type.new(Token.new(:integer, "INTEGER"))

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  describe "eval!(_, %UnaryOp{})" do
    test "returns the value of the underlying expression unchanged when the op is plus", %{
      subj: subj
    } do
      # Setup
      input =
        UnaryOp.new(
          Token.new(:plus, "+"),
          Num.new(Token.new(:integer_const, "42"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {42, _} = ret_val
    end

    test "returns the negated value of the underlying expression when the op is minus", %{
      subj: subj
    } do
      # Setup
      input =
        UnaryOp.new(
          Token.new(:minus, "-"),
          Num.new(Token.new(:integer_const, "42"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {-42, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      input =
        UnaryOp.new(
          Token.new(:plus, "+"),
          Num.new(Token.new(:integer_const, "42"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  describe "eval!(_, %Var{})" do
    test "returns the value of the variable as defined in the call stack", %{subj: subj} do
      # Setup
      subj = put_member(subj, "foo", 42)
      input = Var.new(Token.new(:id, "foo"))

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {42, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      subj = put_member(subj, "foo", 42)
      input = Var.new(Token.new(:id, "foo"))

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  describe "eval!(_, %VarDecl{})" do
    test "returns nil", %{subj: subj} do
      # Setup
      input =
        VarDecl.new(
          Token.new(:id, "foo"),
          Type.new(Token.new(:integer, "INTEGER"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {nil, _} = ret_val
    end

    test "returns the input subject unchanged", %{subj: subj} do
      # Setup
      input =
        VarDecl.new(
          Token.new(:id, "foo"),
          Type.new(Token.new(:integer, "INTEGER"))
        )

      # Execution
      ret_val = Subject.eval!(subj, input)

      # Assertion
      assert {_, ^subj} = ret_val
    end
  end

  @spec put_member(Subject.t(), String.t(), any()) :: Subject.t()
  defp put_member(subj, name, val) do
    updated_stack =
      CallStack.update_hd(subj.call_stack, fn record ->
        ActivationRecord.put_member(record, name, val)
      end)

    subj |> Map.put(:call_stack, updated_stack)
  end
end
