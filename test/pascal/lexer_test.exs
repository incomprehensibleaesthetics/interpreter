defmodule Pascal.LexerTest do
  use ExUnit.Case
  doctest Pascal.Lexer

  alias Pascal.Lexer, as: Subject

  alias GenLexer.Error
  alias GenLexer.Token

  describe "tokenize/1" do
    test "returns a list of tokens based on input code" do
      # Setup
      input = """
      PROGRAM Part11;
      VAR
         number : INTEGER;
         a, b   : INTEGER;
         y      : REAL;

      BEGIN {Part11}
         number := 2;
         a := number ;
         b := 10 * a + 10 * number DIV 4;
         y := 20 / 7 + 3.14
      END.  {Part11}
      """

      # Execution
      {:ok, tokens} = Subject.tokenize(input)

      # Assertion
      assert [
               %Token{name: :program, str: "PROGRAM"},
               %Token{name: :id, str: "Part11"},
               %Token{name: :semi, str: ";"},
               %Token{name: :var, str: "VAR"},
               %Token{name: :id, str: "number"},
               %Token{name: :colon, str: ":"},
               %Token{name: :integer, str: "INTEGER"},
               %Token{name: :semi, str: ";"},
               %Token{name: :id, str: "a"},
               %Token{name: :comma, str: ","},
               %Token{name: :id, str: "b"},
               %Token{name: :colon, str: ":"},
               %Token{name: :integer, str: "INTEGER"},
               %Token{name: :semi, str: ";"},
               %Token{name: :id, str: "y"},
               %Token{name: :colon, str: ":"},
               %Token{name: :real, str: "REAL"},
               %Token{name: :semi, str: ";"},
               %Token{name: :begin, str: "BEGIN"},
               %Token{name: :id, str: "number"},
               %Token{name: :assign, str: ":="},
               %Token{name: :integer_const, str: "2"},
               %Token{name: :semi, str: ";"},
               %Token{name: :id, str: "a"},
               %Token{name: :assign, str: ":="},
               %Token{name: :id, str: "number"},
               %Token{name: :semi, str: ";"},
               %Token{name: :id, str: "b"},
               %Token{name: :assign, str: ":="},
               %Token{name: :integer_const, str: "10"},
               %Token{name: :mul, str: "*"},
               %Token{name: :id, str: "a"},
               %Token{name: :plus, str: "+"},
               %Token{name: :integer_const, str: "10"},
               %Token{name: :mul, str: "*"},
               %Token{name: :id, str: "number"},
               %Token{name: :integer_div, str: "DIV"},
               %Token{name: :integer_const, str: "4"},
               %Token{name: :semi, str: ";"},
               %Token{name: :id, str: "y"},
               %Token{name: :assign, str: ":="},
               %Token{name: :integer_const, str: "20"},
               %Token{name: :float_div, str: "/"},
               %Token{name: :integer_const, str: "7"},
               %Token{name: :plus, str: "+"},
               %Token{name: :real_const, str: "3.14"},
               %Token{name: :end, str: "END"},
               %Token{name: :dot, str: "."},
               %Token{name: :eof, str: ""}
             ] = tokens
    end

    test "returns an error if the code cannot be fully tokenized" do
      # Setup
      input = "foo bar 123 § <- error"

      # Execution / Assertion
      {:error, %Error{}} = Subject.tokenize(input, log_to: nil)
    end
  end

  describe "tokenize!/1" do
    test "returns the same tokens as tokenize on success" do
      # Setup
      input = """
      PROGRAM Part11;
      VAR
         number : INTEGER;
         a, b   : INTEGER;
         y      : REAL;

      BEGIN {Part11}
         number := 2;
         a := number ;
         b := 10 * a + 10 * number DIV 4;
         y := 20 / 7 + 3.14
      END.  {Part11}
      """

      # Execution
      actual_tokens = Subject.tokenize!(input)

      # Assertion
      {:ok, expected_tokens} = Subject.tokenize(input)
      assert expected_tokens == actual_tokens
    end

    test "raises an error if the code cannot be fully tokenized" do
      # Setup
      input = "foo bar 123 § <- error"

      # Execution / Assertion
      assert_raise Error, fn -> Subject.tokenize!(input, log_to: nil) end
    end
  end

  describe "next_token/1 [general]" do
    test "returns the next token if there is one" do
      # Setup
      subj = Subject.new("foo bar")

      # Execution / Assertion
      assert {:ok, {%Token{name: :id, str: "foo"}, _}} = Subject.next_token(subj)
    end

    test "ignores case for keywords" do
      # Setup
      subj = Subject.new("bEGiN")

      # Execution / Assertion
      assert {:ok, {%Token{name: :begin, str: "bEGiN"}, _}} = Subject.next_token(subj)
    end

    test "returns an eof token if there are no more tokens" do
      # Setup
      subj = Subject.new("")

      # Execution / Assertion
      assert {:ok, {%Token{name: :eof, str: ""}, _}} = Subject.next_token(subj)
    end

    test "returns the new lexer state" do
      # Setup
      subj = Subject.new("foo bar")

      # Execution
      {:ok, {_, updated_subj}} = Subject.next_token(subj)

      # Assertion
      assert {:ok, {%Token{name: :id, str: "bar"}, _}} = Subject.next_token(updated_subj)
    end

    test "returns an error if the next token cannot be determined" do
      # Setup
      subj = Subject.new("§1", log_to: nil)

      # Execution / Assertion
      assert {:error, %Error{name: :invalid_token}} = Subject.next_token(subj)
    end

    test "returns the location of the encountered error" do
      # Setup
      subj = Subject.new("foo\n123 §1;\nend", log_to: nil)
      {:ok, {_, subj}} = Subject.next_token(subj)
      {:ok, {_, subj}} = Subject.next_token(subj)

      # Execution / Assertion
      assert {:error, %Error{line_no: 2, col_no: 5}} = Subject.next_token(subj)
    end

    test "sets the position of the returned token" do
      # Setup
      subj = Subject.new("foo\n123 bar;\nend")
      {:ok, {_, subj}} = Subject.next_token(subj)
      {:ok, {_, subj}} = Subject.next_token(subj)

      # Execution / Assertion
      assert {:ok, {%Token{str: "bar", line_no: 2, col_no: 5}, _}} = Subject.next_token(subj)
    end
  end

  describe "next_token/1 [tokens]" do
    test "returns a plus token when appropriate" do
      # Setup
      subj = Subject.new("+foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :plus, str: "+"}, _}} = Subject.next_token(subj)
    end

    test "returns a minus token when appropriate" do
      # Setup
      subj = Subject.new("-foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :minus, str: "-"}, _}} = Subject.next_token(subj)
    end

    test "returns a mul token when appropriate" do
      # Setup
      subj = Subject.new("*foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :mul, str: "*"}, _}} = Subject.next_token(subj)
    end

    test "returns a float_div token when appropriate" do
      # Setup
      subj = Subject.new("/foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :float_div, str: "/"}, _}} = Subject.next_token(subj)
    end

    test "returns a lparen token when appropriate" do
      # Setup
      subj = Subject.new("(foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :lparen, str: "("}, _}} = Subject.next_token(subj)
    end

    test "returns a rparen token when appropriate" do
      # Setup
      subj = Subject.new(")foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :rparen, str: ")"}, _}} = Subject.next_token(subj)
    end

    test "returns a dot token when appropriate" do
      # Setup
      subj = Subject.new(".foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :dot, str: "."}, _}} = Subject.next_token(subj)
    end

    test "returns an assign token when appropriate" do
      # Setup
      subj = Subject.new(":=foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :assign, str: ":="}, _}} = Subject.next_token(subj)
    end

    test "returns a semi token when appropriate" do
      # Setup
      subj = Subject.new(";foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :semi, str: ";"}, _}} = Subject.next_token(subj)
    end

    test "returns a colon token when appropriate" do
      # Setup
      subj = Subject.new(":foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :colon, str: ":"}, _}} = Subject.next_token(subj)
    end

    test "returns a comma token when appropriate" do
      # Setup
      subj = Subject.new(",foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :comma, str: ","}, _}} = Subject.next_token(subj)
    end

    ## Keywords #############################################################

    test "returns a begin token when appropriate" do
      # Setup
      subj = Subject.new("BEGIN foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :begin, str: "BEGIN"}, _}} = Subject.next_token(subj)
    end

    test "returns an integer_div token when appropriate" do
      # Setup
      subj = Subject.new("DIV foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :integer_div, str: "DIV"}, _}} = Subject.next_token(subj)
    end

    test "returns an end token when appropriate" do
      # Setup
      subj = Subject.new("END foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :end, str: "END"}, _}} = Subject.next_token(subj)
    end

    test "returns an integer token when appropriate" do
      # Setup
      subj = Subject.new("INTEGER foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :integer, str: "INTEGER"}, _}} = Subject.next_token(subj)
    end

    test "returns a procedure token when appropriate" do
      # Setup
      subj = Subject.new("PROCEDURE foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :procedure, str: "PROCEDURE"}, _}} = Subject.next_token(subj)
    end

    test "returns a program token when appropriate" do
      # Setup
      subj = Subject.new("PROGRAM foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :program, str: "PROGRAM"}, _}} = Subject.next_token(subj)
    end

    test "returns a real token when appropriate" do
      # Setup
      subj = Subject.new("REAL foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :real, str: "REAL"}, _}} = Subject.next_token(subj)
    end

    test "returns a var token when appropriate" do
      # Setup
      subj = Subject.new("VAR foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :var, str: "VAR"}, _}} = Subject.next_token(subj)
    end

    ## Dynamic Character Sequences ##########################################

    test "returns an integer_const token when appropriate" do
      # Setup
      subj = Subject.new("123 foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :integer_const, str: "123"}, _}} = Subject.next_token(subj)
    end

    test "returns a real_const token when appropriate" do
      # Setup
      subj = Subject.new("123.45.")

      # Execution / Assertion
      assert {:ok, {%Token{name: :real_const, str: "123.45"}, _}} = Subject.next_token(subj)
    end

    test "returns an id token when appropriate" do
      # Setup
      subj = Subject.new("foo BEGINNER")

      # Execution / Assertions

      # This one is simple.
      assert {:ok, {%Token{name: :id, str: "foo"}, _}} = Subject.next_token(subj)

      # Ensure this doesn't get interpreted as the keyword BEGIN.

      token =
        with {:ok, {_, subj}} <- Subject.next_token(subj),
             {:ok, {token, _}} <- Subject.next_token(subj),
             do: token

      assert %Token{name: :id, str: "BEGINNER"} = token
    end
  end

  describe "next_token/1 [ignore behaviour]" do
    test "skips over leading whitespace" do
      # Setup
      subj = Subject.new("  foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :id, str: "foo"}, _}} = Subject.next_token(subj)
    end

    test "skips over a leading comment" do
      # Setup
      subj = Subject.new("{ ignore me }foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :id, str: "foo"}, _}} = Subject.next_token(subj)
    end

    test "skips over all leading whitespace and comments" do
      # Setup
      subj = Subject.new("  { ignore me } { and me too }  foo")

      # Execution / Assertion
      assert {:ok, {%Token{name: :id, str: "foo"}, _}} = Subject.next_token(subj)
    end
  end

  describe "next_tokens!/1" do
    test "returns a lazily evaluated stream of tokens" do
      # Setup
      subj = Subject.new("foo bar § <- error")

      # Execution / Assertion

      # This would raise an error if it wasn't lazy.
      tokens =
        Subject.next_tokens!(subj)
        |> Stream.take(2)
        |> Enum.to_list()

      assert [%Token{str: "foo"}, %Token{str: "bar"}] = tokens
    end

    test "raises an error if tokenization fails" do
      # Setup
      subj = Subject.new("foo bar § <- error", log_to: nil)

      # Execution / Assertion
      assert_raise(Error, "invalid token on line 1 at column 9", fn ->
        Subject.next_tokens!(subj)
        |> Stream.run()
      end)
    end
  end
end
