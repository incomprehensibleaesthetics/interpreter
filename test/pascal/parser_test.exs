defmodule Pascal.ParserTest do
  use ExUnit.Case
  doctest Pascal.Parser

  alias Pascal.Parser, as: Subject

  alias Pascal.Ast
  alias Ast.Assign
  alias Ast.BinOp
  alias Ast.Block
  alias Ast.Compound
  alias Ast.NoOp
  alias Ast.Num
  alias Ast.Param
  alias Ast.ProcCall
  alias Ast.ProcDecl
  alias Ast.Program
  alias Ast.Type
  alias Ast.UnaryOp
  alias Ast.Var
  alias Ast.VarDecl

  alias GenParser.Error
  alias GenLexer.Token

  describe "parse/1 [general]" do
    test "returns a bad_syntax error if the passed token sequence is syntactically incorrect" do
      # PROGRAM foo;
      #
      # BEGIN 1
      # END
      input = [
        # Start with some valid code.
        Token.new(:program, "PROGRAM", pos: {1, 1}),
        Token.new(:id, "foo", pos: {1, 9}),
        Token.new(:semi, ";", pos: {1, 12}),
        Token.new(:begin, "BEGIN", pos: {3, 1}),

        # Error here (statement expected):
        bad_token = Token.new(:integer_const, "1", pos: {3, 7}),

        # More code
        Token.new(:end, "END", pos: {4, 1})
      ]

      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = Subject.parse(input)
    end

    test "returns a missing_eof error if the passed token sequence is missing an eof token at the end" do
      assert {:error, %Error{name: :missing_eof}} = Subject.parse(valid_program_tokens())
    end

    test "returns an unexpected_eof error if the passed token sequence stops unexpectedly" do
      input = [Token.new(:program, "PROGRAM")]
      assert {:error, %Error{name: :unexpected_eof}} = Subject.parse(input)
    end
  end

  describe "parse/2 [rules: assignment_statement]" do
    test "returns an Assign node when appropriate" do
      # Setup
      input =
        [
          Token.new(:id, "x"),
          Token.new(:assign, ":="),
          valid_expr_tokens()
        ]
        |> List.flatten()
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :assignment_statement)

      # Assertion

      expected_ast =
        Assign.new(
          Var.new(Token.new(:id, "x")),
          Token.new(:assign, ":="),
          valid_expr()
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with an assign statement" do
      # Setup
      input = [bad_token = Token.new(:begin, "BEGIN")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :assignment_statement)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: block]" do
    test "returns a Block node when appropriate" do
      # Setup
      input = (valid_declarations_tokens() ++ valid_compound_statement_tokens()) |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :block)

      # Assertion
      expected_ast = Block.new(valid_declarations(), valid_compound_statement())
      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with a block" do
      # Setup
      input =
        [bad_token = Token.new(:id, "foo")]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :block)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: compound_statement]" do
    test "returns a Compound node when appropriate" do
      # Setup

      # BEGIN
      #   {valid_statement_list}
      # END
      input =
        [
          Token.new(:begin, "BEGIN"),
          valid_statement_list_tokens(),
          Token.new(:end, "END")
        ]
        |> append_eof()
        |> List.flatten()

      # Execution
      ret_val = Subject.parse(input, :compound_statement)

      # Assertion
      expected_ast = Compound.new(valid_statement_list())
      assert {:ok, expected_ast} == ret_val
    end

    test "returns an list with a single NoOp node when appropriate" do
      # Setup

      # BEGIN
      #
      # END
      input =
        [
          Token.new(:begin, "BEGIN"),
          Token.new(:end, "END")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :compound_statement)

      # Assertion
      expected_ast = Compound.new([NoOp.new()])
      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with a BEGIN keyword" do
      # Setup
      input =
        [bad_token = Token.new(:id, "foo")]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :compound_statement)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: declarations]" do
    test "returns a list of VarDecl nodes when a single VAR keyword is used" do
      # Setup

      # VAR x, y: INTEGER;
      #     z: REAL;
      input =
        [
          Token.new(:var, "VAR"),
          Token.new(:id, "x"),
          Token.new(:comma, ","),
          Token.new(:id, "y"),
          Token.new(:colon, ":"),
          Token.new(:integer, "INTEGER"),
          Token.new(:semi, ";"),
          Token.new(:id, "z"),
          Token.new(:colon, ":"),
          Token.new(:real, "REAL"),
          Token.new(:semi, ";")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :declarations)

      # Assertion

      expected_ast = [
        %VarDecl{
          name: Token.new(:id, "x"),
          type: Type.new(Token.new(:integer, "INTEGER"))
        },
        %VarDecl{
          name: Token.new(:id, "y"),
          type: Type.new(Token.new(:integer, "INTEGER"))
        },
        %VarDecl{
          name: Token.new(:id, "z"),
          type: Type.new(Token.new(:real, "REAL"))
        }
      ]

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a list of VarDecl nodes when several VAR keywords are used" do
      # Setup

      # VAR x, y: INTEGER;
      # VAR z: REAL;
      input =
        [
          Token.new(:var, "VAR"),
          Token.new(:id, "x"),
          Token.new(:comma, ","),
          Token.new(:id, "y"),
          Token.new(:colon, ":"),
          Token.new(:integer, "INTEGER"),
          Token.new(:semi, ";"),
          Token.new(:var, "VAR"),
          Token.new(:id, "z"),
          Token.new(:colon, ":"),
          Token.new(:real, "REAL"),
          Token.new(:semi, ";")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :declarations)

      # Assertion

      expected_ast = [
        %VarDecl{
          name: Token.new(:id, "x"),
          type: Type.new(Token.new(:integer, "INTEGER"))
        },
        %VarDecl{
          name: Token.new(:id, "y"),
          type: Type.new(Token.new(:integer, "INTEGER"))
        },
        %VarDecl{
          name: Token.new(:id, "z"),
          type: Type.new(Token.new(:real, "REAL"))
        }
      ]

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a list of ProcDecl nodes when appropriate" do
      # Setup

      # PROCEDURE foo;
      #   BEGIN
      #     a := 1
      #   END;
      #
      # PROCEDURE bar;
      #   BEGIN
      #     b := 2
      #   END;
      input =
        [
          Token.new(:procedure, "PROCEDURE", pos: {1, 1}),
          Token.new(:id, "foo"),
          Token.new(:semi, ";"),
          Token.new(:begin, "BEGIN"),
          Token.new(:id, "a"),
          Token.new(:assign, ":="),
          Token.new(:integer_const, "1"),
          Token.new(:end, "END"),
          Token.new(:semi, ";"),
          Token.new(:procedure, "PROCEDURE", pos: {6, 1}),
          Token.new(:id, "bar"),
          Token.new(:semi, ";"),
          Token.new(:begin, "BEGIN"),
          Token.new(:id, "b"),
          Token.new(:assign, ":="),
          Token.new(:integer_const, "2"),
          Token.new(:end, "END"),
          Token.new(:semi, ";")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :declarations)

      # Assertion

      expected_ast = [
        ProcDecl.new(
          Token.new(:id, "foo"),
          [],
          Block.new(
            [],
            Compound.new([
              Assign.new(
                Var.new(Token.new(:id, "a")),
                Token.new(:assign, ":="),
                Num.new(Token.new(:integer_const, "1"))
              )
            ])
          )
        ),
        ProcDecl.new(
          Token.new(:id, "bar"),
          [],
          Block.new(
            [],
            Compound.new([
              Assign.new(
                Var.new(Token.new(:id, "b")),
                Token.new(:assign, ":="),
                Num.new(Token.new(:integer_const, "2"))
              )
            ])
          )
        )
      ]

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a list of both VarDecl and ProcDecl nodes when appropriate" do
      # Setup

      # VAR a, b: INTEGER;
      #
      # PROCEDURE foo;
      #   BEGIN
      #   END;
      #
      # PROCEDURE bar;
      #   BEGIN
      #   END;
      input =
        [
          Token.new(:var, "VAR"),
          Token.new(:id, "a"),
          Token.new(:comma, ","),
          Token.new(:id, "b"),
          Token.new(:colon, ":"),
          Token.new(:integer, "INTEGER"),
          Token.new(:semi, ";"),
          Token.new(:procedure, "PROCEDURE"),
          Token.new(:id, "foo"),
          Token.new(:semi, ";"),
          Token.new(:begin, "BEGIN"),
          Token.new(:end, "END"),
          Token.new(:semi, ";"),
          Token.new(:procedure, "PROCEDURE"),
          Token.new(:id, "bar"),
          Token.new(:semi, ";"),
          Token.new(:begin, "BEGIN"),
          Token.new(:end, "END"),
          Token.new(:semi, ";")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :declarations)

      # Assertion

      expected_ast = [
        VarDecl.new(Token.new(:id, "a"), Type.new(Token.new(:integer, "INTEGER"))),
        VarDecl.new(Token.new(:id, "b"), Type.new(Token.new(:integer, "INTEGER"))),
        ProcDecl.new(
          Token.new(:id, "foo"),
          [],
          Block.new([], Compound.new([NoOp.new()]))
        ),
        ProcDecl.new(
          Token.new(:id, "bar"),
          [],
          Block.new([], Compound.new([NoOp.new()]))
        )
      ]

      assert {:ok, expected_ast} == ret_val
    end

    test "returns an empty list if the token sequence does not start with any declarations" do
      # Setup
      input = [] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :declarations)

      # Assertion
      assert {:ok, []} == ret_val
    end
  end

  describe "parse/2 [rules: expr]" do
    test "returns a single term when appropriate" do
      # Setup
      input = valid_term_tokens() |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :expr)

      # Assertion
      assert {:ok, valid_term()} == ret_val
    end

    test "returns a BinOp node for the addition of two numbers" do
      # Setup
      input =
        [
          Token.new(:integer_const, "1"),
          Token.new(:plus, "+"),
          Token.new(:integer_const, "2")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :expr)

      # Assertion

      expected_ast =
        BinOp.new(
          Num.new(Token.new(:integer_const, "1")),
          Token.new(:plus, "+"),
          Num.new(Token.new(:integer_const, "2"))
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a BinOp node for the subtraction of two numbers" do
      # Setup
      input =
        [
          Token.new(:integer_const, "1"),
          Token.new(:minus, "-"),
          Token.new(:integer_const, "2")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :expr)

      # Assertion

      expected_ast =
        BinOp.new(
          Num.new(Token.new(:integer_const, "1")),
          Token.new(:minus, "-"),
          Num.new(Token.new(:integer_const, "2"))
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a tree of nested BinOp nodes when appropriate" do
      # Setup
      input =
        [
          Token.new(:integer_const, "1"),
          Token.new(:plus, "+"),
          Token.new(:integer_const, "2"),
          Token.new(:minus, "-"),
          Token.new(:integer_const, "3"),
          Token.new(:plus, "+"),
          Token.new(:integer_const, "4")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :expr)

      # Assertion

      expected_ast =
        BinOp.new(
          BinOp.new(
            BinOp.new(
              Num.new(Token.new(:integer_const, "1")),
              Token.new(:plus, "+"),
              Num.new(Token.new(:integer_const, "2"))
            ),
            Token.new(:minus, "-"),
            Num.new(Token.new(:integer_const, "3"))
          ),
          Token.new(:plus, "+"),
          Num.new(Token.new(:integer_const, "4"))
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "handles complex expressions made up of terms and factors" do
      # Setup

      # 1 - 2 * 3 + (-4 + 5)
      input =
        [
          Token.new(:integer_const, "1"),
          Token.new(:minus, "-"),
          Token.new(:integer_const, "2"),
          Token.new(:mul, "*"),
          Token.new(:integer_const, "3"),
          Token.new(:plus, "+"),
          Token.new(:lparen, "("),
          Token.new(:minus, "-"),
          Token.new(:integer_const, "4"),
          Token.new(:plus, "+"),
          Token.new(:integer_const, "5"),
          Token.new(:rparen, ")")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :expr)

      # Assertion

      expected_ast =
        BinOp.new(
          BinOp.new(
            Num.new(Token.new(:integer_const, "1")),
            Token.new(:minus, "-"),
            BinOp.new(
              Num.new(Token.new(:integer_const, "2")),
              Token.new(:mul, "*"),
              Num.new(Token.new(:integer_const, "3"))
            )
          ),
          Token.new(:plus, "+"),
          BinOp.new(
            UnaryOp.new(
              Token.new(:minus, "-"),
              Num.new(Token.new(:integer_const, "4"))
            ),
            Token.new(:plus, "+"),
            Num.new(Token.new(:integer_const, "5"))
          )
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with an expression" do
      # Setup
      input = [bad_token = Token.new(:begin, "BEGIN")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :expr)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: factor]" do
    test "returns a Num node for an integer literal" do
      # Setup
      input = [Token.new(:integer_const, "42")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :factor)

      # Assertion
      expected_ast = Num.new(Token.new(:integer_const, "42"))
      assert {:ok, expected_ast} == ret_val
    end

    test "returns a Num node for a real literal" do
      # Setup
      input = [Token.new(:real_const, "123.45")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :factor)

      # Assertion
      expected_ast = Num.new(Token.new(:real_const, "123.45"))
      assert {:ok, expected_ast} == ret_val
    end

    test "returns a Var node when appropriate" do
      # Setup
      input = [Token.new(:id, "foo")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :factor)

      # Assertion
      expected_ast = Var.new(Token.new(:id, "foo"))
      assert {:ok, expected_ast} == ret_val
    end

    test "returns a UnaryOp node for a positive unary operation" do
      # Setup
      input =
        [
          Token.new(:plus, "+"),
          Token.new(:integer_const, "1")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :factor)

      # Assertion

      expected_ast =
        UnaryOp.new(
          Token.new(:plus, "+"),
          Num.new(Token.new(:integer_const, "1"))
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a UnaryOp node for a negative unary operation" do
      # Setup
      input =
        [
          Token.new(:minus, "-"),
          Token.new(:integer_const, "1")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :factor)

      # Assertion

      expected_ast =
        UnaryOp.new(
          Token.new(:minus, "-"),
          Num.new(Token.new(:integer_const, "1"))
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "handles complex factors including parentheses" do
      # Setup

      # + - - -(1 + 2 * 3 - (6 / 2 + 1))
      input =
        [
          Token.new(:plus, "+"),
          Token.new(:minus, "-"),
          Token.new(:minus, "-"),
          Token.new(:minus, "-"),
          Token.new(:lparen, "("),
          Token.new(:integer_const, "1"),
          Token.new(:plus, "+"),
          Token.new(:integer_const, "2"),
          Token.new(:mul, "*"),
          Token.new(:integer_const, "3"),
          Token.new(:minus, "-"),
          Token.new(:lparen, "("),
          Token.new(:integer_const, "6"),
          Token.new(:float_div, "/"),
          Token.new(:integer_const, "2"),
          Token.new(:plus, "+"),
          Token.new(:integer_const, "1"),
          Token.new(:rparen, ")"),
          Token.new(:rparen, ")")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :factor)

      # Assertion

      expected_ast =
        UnaryOp.new(
          Token.new(:plus, "+"),
          UnaryOp.new(
            Token.new(:minus, "-"),
            UnaryOp.new(
              Token.new(:minus, "-"),
              UnaryOp.new(
                Token.new(:minus, "-"),
                BinOp.new(
                  BinOp.new(
                    Num.new(Token.new(:integer_const, "1")),
                    Token.new(:plus, "+"),
                    BinOp.new(
                      Num.new(Token.new(:integer_const, "2")),
                      Token.new(:mul, "*"),
                      Num.new(Token.new(:integer_const, "3"))
                    )
                  ),
                  Token.new(:minus, "-"),
                  BinOp.new(
                    BinOp.new(
                      Num.new(Token.new(:integer_const, "6")),
                      Token.new(:float_div, "/"),
                      Num.new(Token.new(:integer_const, "2"))
                    ),
                    Token.new(:plus, "+"),
                    Num.new(Token.new(:integer_const, "1"))
                  )
                )
              )
            )
          )
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with a factor" do
      # Setup
      input = [bad_token = Token.new(:begin, "BEGIN")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :factor)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: procedure_call_statement]" do
    test "returns a ProcCall node with an empty list of arguments when appropriate" do
      # Setup
      input =
        [
          Token.new(:id, "greet"),
          Token.new(:lparen, "("),
          Token.new(:rparen, ")")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :procedure_call_statement)

      # Assertion
      expected_ast = ProcCall.new(Token.new(:id, "greet"), [])
      assert {:ok, expected_ast} == ret_val
    end

    test "returns a ProcCall node with a list containing a single argument when appropriate" do
      # Setup
      input =
        [
          Token.new(:id, "consider"),
          Token.new(:lparen, "("),
          Token.new(:integer_const, "42"),
          Token.new(:rparen, ")")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :procedure_call_statement)

      # Assertion

      expected_ast =
        ProcCall.new(
          Token.new(:id, "consider"),
          [Num.new(Token.new(:integer_const, "42"))]
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a ProcCall node with a list of several arguments when appropriate" do
      # Setup
      input =
        [
          Token.new(:id, "multiply"),
          Token.new(:lparen, "("),

          # Sub-expression
          Token.new(:integer_const, "3"),
          Token.new(:plus, "+"),
          Token.new(:integer_const, "5"),
          Token.new(:comma, ","),
          Token.new(:integer_const, "7"),
          Token.new(:rparen, ")")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :procedure_call_statement)

      # Assertion

      expected_ast =
        ProcCall.new(
          Token.new(:id, "multiply"),
          [
            BinOp.new(
              Num.new(Token.new(:integer_const, "3")),
              Token.new(:plus, "+"),
              Num.new(Token.new(:integer_const, "5"))
            ),
            Num.new(Token.new(:integer_const, "7"))
          ]
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with an assign statement" do
      # Setup
      input = [bad_token = Token.new(:begin, "BEGIN")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :procedure_call_statement)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: procedure_declaration]" do
    test "returns a ProcDecl node when appropriate" do
      # Setup

      # PROCEDURE foo;
      #   BEGIN
      #     a := 42
      #   END;
      input =
        [
          Token.new(:procedure, "PROCEDURE"),
          Token.new(:id, "foo"),
          Token.new(:semi, ";"),
          Token.new(:begin, "BEGIN"),
          Token.new(:id, "a"),
          Token.new(:assign, ":="),
          Token.new(:integer_const, "42"),
          Token.new(:end, "END"),
          Token.new(:semi, ";")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :procedure_declaration)

      # Assertion

      expected_ast =
        ProcDecl.new(
          Token.new(:id, "foo"),
          [],
          Block.new(
            [],
            Compound.new([
              Assign.new(
                Var.new(Token.new(:id, "a")),
                Token.new(:assign, ":="),
                Num.new(Token.new(:integer_const, "42"))
              )
            ])
          )
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "supports procedure parameters and variable declarations" do
      # Setup

      # PROCEDURE foo(x, y: INTEGER; z: REAL);
      #   VAR a, b: INTEGER;
      #   BEGIN
      #     a := x
      #   END;
      input =
        [
          Token.new(:procedure, "PROCEDURE"),
          Token.new(:id, "foo"),
          Token.new(:lparen, "("),
          Token.new(:id, "x"),
          Token.new(:comma, ","),
          Token.new(:id, "y"),
          Token.new(:colon, ":"),
          Token.new(:integer, "INTEGER"),
          Token.new(:semi, ";"),
          Token.new(:id, "z"),
          Token.new(:colon, ":"),
          Token.new(:real, "REAL"),
          Token.new(:rparen, ")"),
          Token.new(:semi, ";"),
          Token.new(:var, "VAR"),
          Token.new(:id, "a"),
          Token.new(:comma, ","),
          Token.new(:id, "b"),
          Token.new(:colon, ":"),
          Token.new(:integer, "INTEGER"),
          Token.new(:semi, ";"),
          Token.new(:begin, "BEGIN"),
          Token.new(:id, "a"),
          Token.new(:assign, ":="),
          Token.new(:id, "x"),
          Token.new(:end, "END"),
          Token.new(:semi, ";")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :procedure_declaration)

      # Assertion

      expected_ast =
        ProcDecl.new(
          Token.new(:id, "foo"),
          [
            Param.new(
              Token.new(:id, "x"),
              Type.new(Token.new(:integer, "INTEGER"))
            ),
            Param.new(
              Token.new(:id, "y"),
              Type.new(Token.new(:integer, "INTEGER"))
            ),
            Param.new(
              Token.new(:id, "z"),
              Type.new(Token.new(:real, "REAL"))
            )
          ],
          Block.new(
            [
              VarDecl.new(
                Token.new(:id, "a"),
                Type.new(Token.new(:integer, "INTEGER"))
              ),
              VarDecl.new(
                Token.new(:id, "b"),
                Type.new(Token.new(:integer, "INTEGER"))
              )
            ],
            Compound.new([
              Assign.new(
                Var.new(Token.new(:id, "a")),
                Token.new(:assign, ":="),
                Var.new(Token.new(:id, "x"))
              )
            ])
          )
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "supports nested procedures" do
      # Setup

      # PROCEDURE foo;
      #
      #   PROCEDURE bar;
      #     BEGIN {bar}
      #       b := 2
      #     END; {bar}
      #
      #   BEGIN {foo}
      #     a := 1
      #   END; {foo}
      input =
        [
          Token.new(:procedure, "PROCEDURE"),
          Token.new(:id, "foo"),
          Token.new(:semi, ";"),
          Token.new(:procedure, "PROCEDURE"),
          Token.new(:id, "bar"),
          Token.new(:semi, ";"),
          Token.new(:begin, "BEGIN"),
          Token.new(:id, "b"),
          Token.new(:assign, ":="),
          Token.new(:integer_const, "2"),
          Token.new(:end, "END"),
          Token.new(:semi, ";"),
          Token.new(:begin, "BEGIN"),
          Token.new(:id, "a"),
          Token.new(:assign, ":="),
          Token.new(:integer_const, "1"),
          Token.new(:end, "END"),
          Token.new(:semi, ";")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :procedure_declaration)

      # Assertion

      expected_ast =
        ProcDecl.new(
          Token.new(:id, "foo"),
          [],
          Block.new(
            [
              ProcDecl.new(
                Token.new(:id, "bar"),
                [],
                Block.new(
                  [],
                  Compound.new([
                    Assign.new(
                      Var.new(Token.new(:id, "b")),
                      Token.new(:assign, ":="),
                      Num.new(Token.new(:integer_const, "2"))
                    )
                  ])
                )
              )
            ],
            Compound.new([
              Assign.new(
                Var.new(Token.new(:id, "a")),
                Token.new(:assign, ":="),
                Num.new(Token.new(:integer_const, "1"))
              )
            ])
          )
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with the PROCEDURE keyword" do
      # Setup
      input = [bad_token = Token.new(:id, "foo")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :procedure_declaration)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: program]" do
    test "returns a Program node when appropriate" do
      # Setup

      # PROGRAM foo;
      # {valid_block}
      # .
      input =
        [
          Token.new(:program, "PROGRAM"),
          name_token = Token.new(:id, "foo"),
          Token.new(:semi, ";"),
          valid_block_tokens(),
          Token.new(:dot, ".")
        ]
        |> List.flatten()
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :program)

      # Assertion
      expected_ast = Program.new(name_token, valid_block())
      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with the PROGRAM keyword" do
      # Setup
      input = [bad_token = Token.new(:id, "foo")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :program)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: statement]" do
    test "returns an Assign node when appropriate" do
      # Setup
      input = valid_assign_tokens() |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :statement)

      # Assertion
      assert {:ok, valid_assign()} == ret_val
    end

    test "returns a Compound node when appropriate" do
      # Setup
      input = valid_compound_statement_tokens() |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :statement)

      # Assertion
      assert {:ok, valid_compound_statement()} == ret_val
    end

    test "returns a NoOp node when appropriate" do
      # Setup
      input = [] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :statement)

      # Assertion
      assert {:ok, NoOp.new()} == ret_val
    end

    test "returns a ProcCall node when appropriate" do
      # Setup
      input = valid_proc_call_tokens() |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :statement)

      # Assertion
      assert {:ok, valid_proc_call()} == ret_val
    end
  end

  describe "parse/2 [rules: statement_list]" do
    test "returns a list of statement nodes when appropriate" do
      # Setup

      # a := 1;
      # b := 2;
      input =
        [
          Token.new(:id, "a"),
          Token.new(:assign, ":="),
          Token.new(:integer_const, "1"),
          Token.new(:semi, ";"),
          Token.new(:id, "b"),
          Token.new(:assign, ":="),
          Token.new(:integer_const, "2"),
          Token.new(:semi, ";")
        ]
        |> append_eof()
        |> List.flatten()

      # Execution
      ret_val = Subject.parse(input, :statement_list)

      # Assertion

      expected_ast = [
        Assign.new(
          Var.new(Token.new(:id, "a")),
          Token.new(:assign, ":="),
          Num.new(Token.new(:integer_const, "1"))
        ),
        Assign.new(
          Var.new(Token.new(:id, "b")),
          Token.new(:assign, ":="),
          Num.new(Token.new(:integer_const, "2"))
        ),
        # Required because of the trailing semicolon.
        NoOp.new()
      ]

      assert {:ok, expected_ast} == ret_val
    end

    test "doesn't require the final statement to end with a semicolon" do
      # Setup

      # a := 1;
      # b := 2
      input =
        [
          Token.new(:id, "a"),
          Token.new(:assign, ":="),
          Token.new(:integer_const, "1"),
          Token.new(:semi, ";"),
          Token.new(:id, "b"),
          Token.new(:assign, ":="),
          Token.new(:integer_const, "2")
        ]
        |> append_eof()
        |> List.flatten()

      # Execution
      ret_val = Subject.parse(input, :statement_list)

      # Assertion

      expected_ast = [
        Assign.new(
          Var.new(Token.new(:id, "a")),
          Token.new(:assign, ":="),
          Num.new(Token.new(:integer_const, "1"))
        ),
        Assign.new(
          Var.new(Token.new(:id, "b")),
          Token.new(:assign, ":="),
          Num.new(Token.new(:integer_const, "2"))
        )
      ]

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a list with a single NoOp node when appropriate" do
      # Setup
      input = [] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :statement_list)

      # Assertion
      assert {:ok, [NoOp.new()]} == ret_val
    end
  end

  describe "parse/2 [rules: term]" do
    test "returns a single factor when appropriate" do
      # Setup
      input = valid_factor_tokens() |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :term)

      # Assertion
      assert {:ok, valid_factor()} == ret_val
    end

    test "returns a BinOp node for the multiplication of two numbers" do
      # Setup
      input =
        [
          Token.new(:integer_const, "1"),
          Token.new(:mul, "*"),
          Token.new(:integer_const, "2")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :term)

      # Assertion

      expected_ast =
        BinOp.new(
          Num.new(Token.new(:integer_const, "1")),
          Token.new(:mul, "*"),
          Num.new(Token.new(:integer_const, "2"))
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a BinOp node for the integer division of two numbers" do
      # Setup
      input =
        [
          Token.new(:integer_const, "1"),
          Token.new(:integer_div, "DIV"),
          Token.new(:integer_const, "2")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :term)

      # Assertion

      expected_ast =
        BinOp.new(
          Num.new(Token.new(:integer_const, "1")),
          Token.new(:integer_div, "DIV"),
          Num.new(Token.new(:integer_const, "2"))
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a BinOp node for the float division of two numbers" do
      # Setup
      input =
        [
          Token.new(:integer_const, "1"),
          Token.new(:float_div, "/"),
          Token.new(:integer_const, "2")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :term)

      # Assertion

      expected_ast =
        BinOp.new(
          Num.new(Token.new(:integer_const, "1")),
          Token.new(:float_div, "/"),
          Num.new(Token.new(:integer_const, "2"))
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a tree of nested BinOp nodes when appropriate" do
      # Setup
      input =
        [
          Token.new(:integer_const, "1"),
          Token.new(:mul, "*"),
          Token.new(:integer_const, "2"),
          Token.new(:mul, "*"),
          Token.new(:integer_const, "3"),
          Token.new(:mul, "*"),
          Token.new(:integer_const, "4")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :term)

      # Assertion

      expected_ast =
        BinOp.new(
          BinOp.new(
            BinOp.new(
              Num.new(Token.new(:integer_const, "1")),
              Token.new(:mul, "*"),
              Num.new(Token.new(:integer_const, "2"))
            ),
            Token.new(:mul, "*"),
            Num.new(Token.new(:integer_const, "3"))
          ),
          Token.new(:mul, "*"),
          Num.new(Token.new(:integer_const, "4"))
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "handles complex terms made up of factors" do
      # Setup

      # 2 * -3 / (1 + 1)
      input =
        [
          Token.new(:integer_const, "2"),
          Token.new(:mul, "*"),
          Token.new(:minus, "-"),
          Token.new(:integer_const, "3"),
          Token.new(:float_div, "/"),
          Token.new(:lparen, "("),
          Token.new(:integer_const, "1"),
          Token.new(:plus, "+"),
          Token.new(:integer_const, "1"),
          Token.new(:rparen, ")")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :term)

      # Assertion

      expected_ast =
        BinOp.new(
          BinOp.new(
            Num.new(Token.new(:integer_const, "2")),
            Token.new(:mul, "*"),
            UnaryOp.new(
              Token.new(:minus, "-"),
              Num.new(Token.new(:integer_const, "3"))
            )
          ),
          Token.new(:float_div, "/"),
          BinOp.new(
            Num.new(Token.new(:integer_const, "1")),
            Token.new(:plus, "+"),
            Num.new(Token.new(:integer_const, "1"))
          )
        )

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with a term" do
      # Setup
      input = [bad_token = Token.new(:begin, "BEGIN")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :term)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: type_spec]" do
    test "returns a Type node for an INTEGER" do
      # Setup
      input =
        [token = Token.new(:integer, "INTEGER")]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :type_spec)

      # Assertion
      assert {:ok, %Type{token: ^token}} = ret_val
    end

    test "returns a Type node for a REAL" do
      # Setup
      input =
        [token = Token.new(:real, "REAL")]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :type_spec)

      # Assertion
      assert {:ok, %Type{token: ^token}} = ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with a type_spec" do
      # Setup
      input =
        [bad_token = Token.new(:id, "foo")]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :type_spec)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: variable]" do
    test "returns a Var node when appropriate" do
      # Setup
      input = [token = Token.new(:id, "foo")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :variable)

      # Assertion
      expected_ast = Var.new(token)
      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't begin with an id token" do
      # Setup
      input = [bad_token = Token.new(:begin, "BEGIN")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :variable)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse/2 [rules: variable_declaration]" do
    test "returns a list with a single VarDecl node when appropriate" do
      # Setup
      input =
        [
          name_token = Token.new(:id, "foo"),
          Token.new(:colon, ":"),
          type_token = Token.new(:integer, "INTEGER")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :variable_declaration)

      # Assertion
      expected_ast = [VarDecl.new(name_token, Type.new(type_token))]
      assert {:ok, expected_ast} == ret_val
    end

    test "returns a list with several VarDecl nodes when appropriate" do
      # Setup
      input =
        [
          name_token_1 = Token.new(:id, "one"),
          Token.new(:comma, ","),
          name_token_2 = Token.new(:id, "two"),
          Token.new(:comma, ","),
          name_token_3 = Token.new(:id, "three"),
          Token.new(:colon, ":"),
          type_token = Token.new(:integer, "INTEGER")
        ]
        |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :variable_declaration)

      # Assertion

      expected_ast = [
        VarDecl.new(name_token_1, Type.new(type_token)),
        VarDecl.new(name_token_2, Type.new(type_token)),
        VarDecl.new(name_token_3, Type.new(type_token))
      ]

      assert {:ok, expected_ast} == ret_val
    end

    test "returns a bad_syntax error if the passed token sequence doesn't start with an id" do
      # Setup
      input = [bad_token = Token.new(:begin, "BEGIN")] |> append_eof()

      # Execution
      ret_val = Subject.parse(input, :variable_declaration)

      # Assertion
      assert {:error, %Error{name: :bad_syntax, bad_token: ^bad_token}} = ret_val
    end
  end

  describe "parse!/1" do
    test "returns the same ast as parse on success" do
      # Setup

      # PROGRAM foo;
      #
      # BEGIN
      # END.
      input =
        [
          Token.new(:program, "PROGRAM", pos: {1, 1}),
          Token.new(:id, "foo", pos: {1, 9}),
          Token.new(:semi, ";", pos: {1, 12}),
          Token.new(:begin, "BEGIN", pos: {3, 1}),
          Token.new(:end, "END", pos: {4, 1}),
          Token.new(:dot, ".", pos: {4, 4})
        ]
        |> append_eof()

      # Execution
      actual_ast = Subject.parse!(input)

      # Assertion
      {:ok, expected_ast} = Subject.parse(input)
      assert expected_ast == actual_ast
    end

    test "raises an error if parsing fails" do
      # Setup
      input = [Token.new(:id, "foo")]

      # Execution / Assertion
      assert_raise Error, fn -> Subject.parse!(input) end
    end
  end

  defp append_eof(tokens), do: tokens ++ [Token.new(:eof, nil)]

  defp valid_assign do
    Assign.new(
      Var.new(Token.new(:id, "x")),
      Token.new(:assign, ":="),
      Num.new(Token.new(:integer_const, "42"))
    )
  end

  defp valid_assign_tokens do
    [
      Token.new(:id, "x"),
      Token.new(:assign, ":="),
      Token.new(:integer_const, "42")
    ]
  end

  defp valid_block, do: Block.new(valid_declarations(), valid_compound_statement())
  defp valid_block_tokens, do: valid_declarations_tokens() ++ valid_compound_statement_tokens()
  defp valid_compound_statement, do: Compound.new([NoOp.new()])
  defp valid_compound_statement_tokens, do: [Token.new(:begin, "BEGIN"), Token.new(:end, "END")]

  defp valid_declarations do
    [
      VarDecl.new(
        Token.new(:id, "x"),
        Type.new(Token.new(:integer, "INTEGER"))
      )
    ]
  end

  defp valid_declarations_tokens do
    [
      Token.new(:var, "VAR"),
      Token.new(:id, "x"),
      Token.new(:colon, ":"),
      Token.new(:integer, "INTEGER"),
      Token.new(:semi, ";")
    ]
  end

  defp valid_expr, do: Num.new(Token.new(:integer_const, "42"))
  defp valid_expr_tokens, do: [Token.new(:integer_const, "42")]
  defp valid_factor, do: Num.new(Token.new(:integer_const, "42"))
  defp valid_factor_tokens, do: [Token.new(:integer_const, "42")]
  defp valid_statement_list, do: [valid_assign()]
  defp valid_statement_list_tokens, do: [valid_assign_tokens()]

  defp valid_proc_call, do: ProcCall.new(Token.new(:id, "calc"), [])

  defp valid_proc_call_tokens do
    [
      Token.new(:id, "calc"),
      Token.new(:lparen, "("),
      Token.new(:rparen, ")")
    ]
  end

  defp valid_program_tokens do
    List.flatten([
      Token.new(:program, "PROGRAM"),
      Token.new(:id, "foo"),
      Token.new(:semi, ";"),
      valid_block_tokens(),
      Token.new(:dot, ".")
    ])
  end

  defp valid_term, do: Num.new(Token.new(:integer_const, "42"))
  defp valid_term_tokens, do: [Token.new(:integer_const, "42")]
end
